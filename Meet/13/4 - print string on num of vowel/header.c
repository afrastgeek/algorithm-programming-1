#include "header.h"

int calculateVowel(int n, char string[n][64]) {
    int numOfVowel = 0;
    for (index1 = 0; index1 < n; index1 += 1) {
        for (index2 = 0; index2 < strlen(string[index1]); index2 += 1) {
            if (string[index1][index2] == 'a' || string[index1][index2] == 'i'
                || string[index1][index2] == 'u' || string[index1][index2] == 'e'
                || string[index1][index2] == 'o') {
                numOfVowel += 1;
            }
        }
    }
    return numOfVowel;
}

void display(int n, int m, char string[m][64]) {
    for (index1 = 0; index1 < n; index1 += 1) {
        for (index2 = 0; index2 < m; index2 += 1) {
            printf("%s ", string[index2]);
        }
    }
}