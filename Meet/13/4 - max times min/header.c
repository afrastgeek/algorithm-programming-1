#include "header.h"

int nilaiMinimal(int n, int arr[n]) {
    int index, min = arr[0];
    for (index = 0; index < n; index += 1) {
        if (min > arr[index]) {
            min = arr[index];
        }
    }
    return min;
}

int nilaiMaksimal(int n, int arr[n]) {
    int index, max = arr[0];
    for (index = 0; index < n; index += 1) {
        if (max < arr[index]) {
            max = arr[index];
        }
    }
    return max;
}

void cetakN(int n) {
    int index;
    for (index = 0; index < n; index += 1) {
        printf("Terima Kasih untuk Tetap Semangat ");
    }
}