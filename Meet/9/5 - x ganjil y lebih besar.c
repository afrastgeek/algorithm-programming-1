#include <stdio.h>

typedef struct {
    int x;
    int y;
} titik;

int main()
{
    int max, index;
    scanf("%d", &max);
    titik koordinat[max];
    for (index = 0; index < max; index++)
    {
        scanf("%d", &koordinat[index].x);
        scanf("%d", &koordinat[index].y);
    }
    
    index = 0;
    while(index < max) {
        if ((koordinat[index].x %2 == 1) && (koordinat[index].y > koordinat[index].x))
        {
            printf("%d %d\n", koordinat[index].x, koordinat[index].y);
        }
        index += 1;
    }

    return 0;
}