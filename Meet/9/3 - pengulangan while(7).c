#include <stdio.h>

int main()
{
    int max, index, even = 0
    , counter = 0;

    scanf("%d", &max);

    int tabInt[max];

    for (index = 0; index < max; index += 1)
    {
        scanf("%d", &tabInt[index]);
    }

    while ((counter < max) && (even <= 3))
    {
        if (tabInt[counter] % 2 == 0)
        {
            printf("%d\n", tabInt[counter]);
            even += 1;
        }
        counter += 1;
    }


    return 0;
}