// Program sederhana untuk operasi luas segitiga
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>
#include <math.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	float alas;
	float tinggi;

	// membuat variabel untuk rumus
	float rumus = 0.5;

	//membuat variabel untuk output
	float luas;

	// menampilkan pesan nama program
	printf("Penghitung luas segitiga\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan panjang alas\n");
	scanf("%f", &alas);
	printf("Masukkan panjang tinggi\n");
	scanf("%f", &tinggi);

	// KALKULASI
	luas = rumus * alas * tinggi;

	// OUTPUT
	printf("Luas segitiganya %f\n",luas);

	return 0;
}