// Program sederhana untuk operasi jarak dua titik
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>
#include <math.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	int x1;
	int x2;
	int y1;
	int y2;

	// membuat variabel untuk rumus
	int minx;
	int miny;

	//membuat variabel untuk output
	int jarak;

	// menampilkan pesan nama program
	printf("Penghitung luas antar titik\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan x1\n");
	scanf("%d", &x1);
	printf("Masukkan x2\n");
	scanf("%d", &x2);
	printf("Masukkan y1\n");
	scanf("%d", &y1);
	printf("Masukkan y2\n");
	scanf("%d", &y2);

	// KALKULASI
	minx = x1-x2;
	miny = y1-y2;
	jarak = sqrt(minx * minx) + (miny * miny);

	// OUTPUT
	printf("Jarak antar dua titik %d\n",jarak);

	return 0;
}