// Program sederhana untuk operasi extraksi angka
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>

// main function
int main()
{
	// membuat variabel untuk input
	int angka;

	//membuat variabel untuk output
	int ribuan;
	int ratusan;
	int puluhan;
	int satuan;

	// menampilkan pesan nama program
	printf("Ekstraktor angka hingga ribuan\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan angka\n");
	scanf("%d", &angka);

	// KALKULASI
	ribuan = angka / 1000;
	ratusan = (angka / 100) % 10;
	puluhan = (angka / 10) % 10;
	satuan = angka % 10;

	// OUTPUT
	printf("Angka %d\nRibuan %d\nRatusan %d\nPuluhan %d\nSatuan %d\n",angka, ribuan, ratusan, puluhan, satuan);

	return 0;
}