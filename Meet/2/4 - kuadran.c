// Program sederhana untuk operasi kuadrat
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>
#include <math.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	int angka;

	//membuat variabel untuk output
	int kuadran;
	int akar;

	// menampilkan pesan nama program
	printf("Penghitung operasi kuadrat\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan bilangan integer\n");
	scanf("%d", &angka);

	// KALKULASI
	kuadran = angka * angka;

	akar = sqrt(angka);

	// OUTPUT
	printf("Angka %d\ndikuadratkan menjadi %d\ndan diakarkan menjadi %d\n",angka,kuadran,akar);

	return 0;
}