// Program sederhana untuk perkalian
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	int angka1;
	int angka2;

	//membuat variabel untuk output
	int angka3;

	// menampilkan pesan nama program
	printf("Perkalian dua bilangan\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan angka pertama\n");
	scanf("%d", &angka1);

	printf("Masukkan angka kedua\n");
	scanf("%d", &angka2);

	// KALKULASI
	angka3 = angka1 * angka2;

	// OUTPUT
	printf("Hasil Perkalian dari %d dan %d adalah: %d\n",angka1,angka2,angka3);

	return 0;
}