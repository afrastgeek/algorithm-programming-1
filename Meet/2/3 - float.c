// Program sederhana untuk memisahkan float
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	float riil;

	//membuat variabel untuk output
	int angka_depan;
	int angka_belakang;

	// menampilkan pesan nama program
	printf("Pemisah bilangan riil dari koma\n");

	// INPUT
	// menampilkan pesan agar user melakukan input
	printf("Masukkan bilangan riil\n");
	scanf("%f", &riil);

	// KALKULASI
	angka_depan = riil;

	angka_belakang = (riil - angka_depan) * 100;

	// OUTPUT
	printf("Angka depan %d dan angka belakang %d\n",angka_depan,angka_belakang);

	return 0;
}