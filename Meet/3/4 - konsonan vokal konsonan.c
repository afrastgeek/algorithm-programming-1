#include <stdio.h>

int main()
{
	// declare variable
	char ca, cb, cc;
	int a=0, b=0, c=0;

	// input
	scanf("%c %c %c", &ca, &cb, &cc);

	// check first character, is it consonant?
	if (ca != 'a' && ca != 'i' && ca != 'u' && ca != 'e' && ca != 'o')
	{
		a = 1;
	}

	// check second character, is it vocal?
	if (cb == 'a' || cb == 'i' || cb == 'u' || cb == 'e' || cb == 'o')
	{
		b = 1;
	}

	// check third character, is it consonant?
	if (cc != 'a' && cc != 'i' && cc != 'u' && cc != 'e' && cc != 'o')
	{
		c = 1;
	}

	// conditional
	// is the input consonant - vocal - consonant?
	if (a == 1 && b == 1 && c ==1)
	{
		printf("memenuhi syarat");
	} else {
		printf("tidak memenuhi syarat");
	}

	return 0;
}