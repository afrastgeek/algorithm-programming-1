#include "pola.h"

int main(int argc, char const *argv[])
{
    int b;
    scanf("%d", &b);

    pola(b);
    pola(b + 1);
    pola(b + 2);

    int kumpulan[b];

    int i;
    for (i = 0; i < b; i += 1)
    {
        scanf("%d", &kumpulan[i]);
    }

    tulisArrayInt(b, kumpulan);

    return 0;
}