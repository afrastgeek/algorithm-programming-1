#include <stdio.h>

int main()
{
	int multiplier, index;

	scanf("%d", &multiplier);

	for (index = 0; index < multiplier; index += 1)
	{
		printf("* ");
	}

	return 0;
}