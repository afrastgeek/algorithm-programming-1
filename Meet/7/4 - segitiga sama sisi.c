#include <stdio.h>

int main()
{
	int multiplier, index, index2, i;

	scanf("%d", &multiplier);

	for (index = 0; index < multiplier; index +=1)
	{
		printf("\n");
		for (i = multiplier; i > index; i -= 1)
			{
				printf(" ");
			}
		for (index2 = 0; index2 <= index; index2 += 1)
		{
			printf("* ");
		}
	}

	return 0;
}