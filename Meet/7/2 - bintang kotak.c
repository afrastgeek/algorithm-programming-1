#include <stdio.h>

int main()
{
	int multiplier, index, index2;

	scanf("%d", &multiplier);

	for (index = 0; index < multiplier; index += 1)
	{
		printf("\n");
		for (index2 = 0; index2 < multiplier; index2 += 1)
		{
			printf("* ");
		}
	}

	return 0;
}