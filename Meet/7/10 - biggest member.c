#include <stdio.h>

int main()
{
	int number;

	scanf ("%d", &number);

	int tabint[number];
	int index;
	int max = 0;

	for (index = 0; index < number; index += 1)
	{
		scanf("%d", &tabint[index]);
	}

	for (index = 0; index < number; index += 1)
	{
		if (max < tabint[index])
		{
			max = tabint[index];
		}
	}

	printf("%d\n", max);

	return 0;
}