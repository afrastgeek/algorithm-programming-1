#include <stdio.h>

int main()
{
	int number;

	scanf ("%d", &number);

	int tabint[number];
	int index;

	for (index = 0; index < number; index += 1)
	{
		scanf("%d", &tabint[index]);
	}

	for (index = 0; index < (number/2); index += 1)
	{
		printf("%d ", tabint[index]);
	}

	return 0;
}