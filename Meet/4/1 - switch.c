#include <stdio.h>

int main()
{
	int hari;
	scanf("%d", &hari);

	switch(hari){
		case 1 : {
			printf("hari senin\n");
			break;
		}
		case 2 : {
			printf("hari selasa\n");
			break;
		}
		case 3 : {
			printf("hari rabu\n");
			break;
		}
		case 4 : {
			printf("hari kamis\n");
			break;
		}
		case 5 : {
			printf("hari jum\'at\n");
			break;
		}
		case 6 : {
			printf("hari sabtu\n");
			break;
		}
		case 7 : {
			printf("hari minggu\n");
			break;
		}
		default : {
			printf("tidak ada hari ke : %d\n", hari);
			break;
		}
	}

	return 0;
}