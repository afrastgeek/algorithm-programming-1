#include <stdio.h>

typedef struct{
	int pembilang;
	int penyebut;
}pecahan;

int main()
{
	pecahan p1;
	pecahan p2;
	pecahan p3;

	printf("Masukkan pecahan pertama dan kedua (pecahan dan pembilang berturut-turut)\n");
	scanf("%d %d %d %d", &p1.pembilang, &p1.penyebut, &p2.pembilang, &p2.penyebut);

	p3.pembilang = p1.pembilang * p2.pembilang;
	p3.penyebut = p1.penyebut * p2.penyebut;

	printf("Hasil kali %d/%d dan %d/%d ialah %d/%d\n", p1.pembilang, p1.penyebut, p2.pembilang, p2.penyebut, p3.pembilang, p3.penyebut);

	return 0;
}