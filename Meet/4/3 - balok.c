#include <stdio.h>

typedef struct{
	int panjang;
	int lebar;
	int tinggi;
	int luaspermukaan;
	int volume;
}balok;

int main()
{
	balok b;

	printf("Masukan nilai panjang, lebar, tinggi\n");
	scanf("%d %d %d", &b.panjang, &b.lebar, &b.tinggi);

	b.luaspermukaan = 2 * ((b.panjang * b.lebar) + (b.panjang * b.tinggi) + (b.lebar * b.tinggi));
	b.volume = b.panjang * b.lebar * b.tinggi;

	printf("Hasil luas permukaan: %d\n", b.luaspermukaan);
	printf("Hasil volume: %d\n", b.volume);

	return 0;
}