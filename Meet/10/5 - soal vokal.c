#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int count = 0, index;
    char s[64];

    scanf("%s", &s);
    int len = strlen(s);

    for (index = 0; index < len; index += 1)
    {
        if (s[index] == 'a' || s[index] == 'i' || s[index] == 'u' || s[index] == 'e' || s[index] == 'o')
        {
            count += 1;
        }
    }

    printf("%d\n", count);

    return 0;
}