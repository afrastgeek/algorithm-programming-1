#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char s[64];

    scanf("%s", &s);
    
    int index;

    for (index = 0; index < strlen(s); index += 1)
    {
        printf("%c\n", s[index]);
    }

    return 0;
}