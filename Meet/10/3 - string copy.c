#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char s1[64];
    char s2[64];

    scanf("%s", &s1);
    
    strcpy(s2, s1);

    printf("%s\n", s1);
    printf("%s\n", s2);

    return 0;
}