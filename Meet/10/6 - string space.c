#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char s[64];

    scanf("%s", &s);
    
    int index, anotherindex, len;
    len = strlen(s);

    for (index = 0; index < len; index += 1)
    {
        for (anotherindex = 0; anotherindex < index; anotherindex += 1)
        {
            printf(" ");
        }
        printf("%c\n", s[index]);
    }

    for (index = 0; index < len; index += 1)
    {
        for (anotherindex = len -1; anotherindex > index; anotherindex -= 1)
        {
            printf(" ");
        }
        printf("%c\n", s[index]);
    }

    return 0;
}