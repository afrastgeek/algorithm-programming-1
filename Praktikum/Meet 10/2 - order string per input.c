#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int numofstring, index = 0, index2;

    scanf("%d", &numofstring);

    char string[numofstring][64];

    while (index < numofstring)
    {
        scanf(" %s", &string[index]);
        index += 1;
    }

    index = 0;
    while (index < numofstring)    
    {
        if (index % 2 == 0)
        {
            printf("%d %s\n", index + 1, string[index]);
        }
        index += 1;
    }
    
    index = 0;
    while (index < numofstring)    
    {
        if (index % 2 != 0)
        {
            printf("%d %s\n", index + 1, string[index]);
        }
        index += 1;
    }
    
    return 0;
}