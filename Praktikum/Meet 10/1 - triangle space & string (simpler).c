#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int i = 0, j;
    char g[64];

    scanf("%s", &g);

    while (g[i] != '\0')
    {
        j = 0;
        while (j < i)
        {
            printf(" ");
            j += 1;
        }
        printf("%c\n", g[i]);
        i += 1;   
    }

    return 0;
}