#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int index, index2, length;
    char string[64];

    scanf("%s", &string);

    length = strlen(string);

    for (index = 0; index < length; index += 1)
    {
        for (index2 = 0; index2 < index; index2 += 1)
        {
            printf(" ");
        }
        printf("%c\n", string[index]);
    }
    
    return 0;
}