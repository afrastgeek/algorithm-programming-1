#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int vocal, cons, sum, length, multiply, index;
    char g[100];

    vocal = cons = 0;

    scanf(" %s", &g);
    length = strlen(g);

    for (index = 0; index < length; index += 1)
    {
        if (g[index] == 'a' || g[index] == 'e' || g[index] == 'i' || g[index] == 'o' || g[index] == 'u')
        {
            vocal += 1;
        } else
        {
            cons += 1;
        }
    }

    sum = vocal * cons;

    if (sum % 2 == 0)
    {
        multiply = 4;
    } else
    {
        multiply = 3;
    }

    for (index = 0; index < length; index += multiply)
    {
        if (sum % 2 == 0)
        {
            printf("%c%c%c%c\n", g[index], g[index + 1], g[index + 2], g[index + 3]);
        } else
        {
            printf("%c%c%c\n", g[index], g[index + 1], g[index + 2]);
        }
    }

    return 0;
}