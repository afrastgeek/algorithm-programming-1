#include <stdio.h>
#include <string.H>

int main(int argc, char const *argv[])
{
    int caps, lower, index;
    char wildcard[512];

    caps = lower = 0;

    scanf("%s", &wildcard);

    for (index = 0; index < strlen(wildcard); index += 1)
    {
        if ((wildcard[index] >= 65) && (wildcard[index] <= 90))
        {
            caps += 1;
        } else if ((wildcard[index] >= 97) && (wildcard[index] <= 122))
        {
            lower += 1;
        }
    }

    printf("Banyak huruf kapital : %d\n", caps);
    printf("Banyak huruf kecil : %d\n", lower);

    return 0;
}