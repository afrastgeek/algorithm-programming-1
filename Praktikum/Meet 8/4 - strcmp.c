#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char array1[100];
    scanf("%s", &array1);

    char array2[100];
    scanf("%s", &array2);

    /* sebelum copy process */
    if (strcmp(array1, array2) == 1)
    {
        printf("%s\n", array2);
        printf("%s\n", array1);
    } else
    {
        printf("%s\n", array1);
        printf("%s\n", array2);
    }

    return 0;
}