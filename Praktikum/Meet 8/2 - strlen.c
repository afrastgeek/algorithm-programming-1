#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char array[100];
    scanf("%s", &array);

    /* langsung di print */
    printf("%d\n", strlen(array));

    /* ditampung ke variable */
    int n = strlen(array);
    printf("%d\n", n);

    return 0;
}