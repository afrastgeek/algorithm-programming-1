#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char array1[100];
    char array2[100];
    scanf("%s", &array1);

    /* sebelum copy process */
    printf("ini array2: %s\n", array2);

    strcpy(array2, array1);

    /* setelah pengcopyan */
    printf("ini array2: %s\n", array2);

    return 0;
}