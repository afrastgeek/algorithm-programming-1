#include <stdio.h>

int main(int argc, char const *argv[])
{
    int angka;
    int jumlah = 0;

    // perulangan
    do{
        scanf("%d", &angka); // inputan
        jumlah = jumlah + angka; // menjumlahkan angka
    } while (angka != 0);

    printf("jumlahnya adalah: %d\n", jumlah);

    return 0;
}