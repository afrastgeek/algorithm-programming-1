/*
    Di warung Pa Engkus Moni akan jajan tango, AQUA, dan gehu.
    Bantulah moni untuk menghitung total yang dibelinya:
    tango   : 1000
    AQUA    : 3000
    gehu    : 500
    * masukan akan berakhir jika moni berkata "sudah"
 */
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char item[8];
    int sum = 0;

    do
    {
        scanf("%s", &item);
        if (strcmp(item,"tango") == 0)
        {
            sum += 1000;
        } else if (strcmp(item,"AQUA") == 0)
        {
            sum += 3000;
        } else if (strcmp(item,"gehu") == 0)
        {
            sum += 500;
        }
    } while(strcmp(item, "sudah") != 0);

    printf("%d\n", sum);

    return 0;
}