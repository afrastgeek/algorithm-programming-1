#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int n; //banyaknya string
    int i, j; //counter
    int ketemu; //status
    int total; //total jumlah kata alay

    scanf("%d", &n); //masukan jumlah string

    char arrstr[n][50]; //array of string

    //proses masukan
    for (i = 0; i < n; i += 1)
    {
        scanf("%s", &arrstr[i]);
    }

    total = 0; //total kata alay diinisialisasi

    /* proses penghitugan kata alay dalam array of string */
    for (i = 0; i < n; i += 1)
    {
        j = 0;
        ketemu = 0;
        while(j<strlen(arrstr[i]) && ketemu == 0)
        {
            if ((arrstr[i][j] == '1') || (arrstr[i][j] == '2') || (arrstr[i][j] == '3') || (arrstr[i][j] == '4') || (arrstr[i][j] == '5') || (arrstr[i][j] == '6') || (arrstr[i][j] == '7') || (arrstr[i][j] == '8') || (arrstr[i][j] == '9') || (arrstr[i][j] == '0') )

            {
                ketemu = 1;

                //jumlah total bertambah
                total += 1;
            } else
            {
                //iterasi
                j += 1;
            }
        }
    }

    printf("%d\n", total);

    return 0;
}