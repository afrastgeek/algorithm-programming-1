#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char string[128];

    scanf("%s", &string);

    int i;
    int count = 0;

    for (i = 0; i < strlen(string); i += 1)
    {
        if (string[i] == 'a' ||
            string[i] == 'i' ||
            string[i] == 'u' ||
            string[i] == 'e' ||
            string[i] == 'o')
        {
            // with help from ascii:
            // string[i] = count % 10 + 48;
            count %= 10;

            switch (count)
            {
                case 0 : str[i] = '0'; break;
                case 1 : str[i] = '1'; break;
                case 2 : str[i] = '2'; break;
                case 3 : str[i] = '3'; break;
                case 4 : str[i] = '4'; break;
                case 5 : str[i] = '5'; break;
                case 6 : str[i] = '6'; break;
                case 7 : str[i] = '7'; break;
                case 8 : str[i] = '8'; break;
                case 9 : str[i] = '9'; break;
            }

            count += 1;
        }
    }

    printf("%s\n", string);

    return 0;
}