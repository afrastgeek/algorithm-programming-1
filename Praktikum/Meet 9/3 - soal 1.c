#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int n;
    int i;
    scanf("%d", &n);

    char str[n][50];

    for (i = 0; i < n; i += 1)
    {
        scanf("%s", &str[i]);
    }

    int panjangmaks;
    panjangmaks = strlen(str[0]);

    for (i = 0; i < n; i += 1)
    {
        if (panjangmaks < strlen(str[i]))
        {
            panjangmaks = strlen(str[i]);
        }
    }

    int j;
    //proses output
    for (i = 0; i < panjangmaks; i += 1)
    {
        for (j = 0; j < n; j += 1)
        {
            if (i > strlen(str[j]))
            {
                printf(" ");
            } else
            {
                printf("%c", str[j][i]);
            }
        }
        printf("\n");
    }

    return 0;
}