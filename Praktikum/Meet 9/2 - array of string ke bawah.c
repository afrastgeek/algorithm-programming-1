#include <stdio.h>
#include <string.h>

typedef struct { //deklarasi bungkusan
    char kata[50] //string
} string; //nama bungkusan

int main(int argc, char const *argv[])
{

    int n; //jumlah baris string
    int i, j; //penghitung

    scanf("%d", &n);
    string arr[n]; //deklarasi array of bungkusan string
    //proses masukan
    for (i = 0; i < n; i += 1)
    {
        scanf("%s", &arr[i].kata);
    }
    //proses menampilkan output
    printf("\n");
    for (i = 0; i < n; i += 1)
    {
        for (j = 0; j < strlen(arr[i].kata); j += 1)
        {
            printf("%c\n", arr[i].kata[j]);
        }
    }

    return 0;
}