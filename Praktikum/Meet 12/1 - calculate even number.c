#include <stdio.h>

int isEven (int angka) {
    if (angka % 2 == 0) {
        /* True/genap */
        return 1;
    } else {
        /* False/ganjil */
        return 0;
    }
}

/* void main */
int main(int argc, char const *argv[]) {
    int n;
    scanf("%d", &n);
    int i;
    // int hasil;
    int genap = 0;
    int arrInt[n];
    for (int i = 0; i < n; i += 1) {
        scanf("%d", &arrInt[i])
        // hasil = isEven(arrInt[i]);
        // if (hasil == 1) {
        //     genap += 1;
        // }
        /*-----------------*/
        if (isEven(arrInt[i]) == 1) {
            genap += 1;
        }
    }

    printf("%d\n", genap);

    return 0;
}