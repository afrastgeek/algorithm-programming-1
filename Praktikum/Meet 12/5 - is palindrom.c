#include <stdio.h>
#include <string.h>

int isPalindrom (char theString[]) {
    int halfLength, counter, index;

    halfLength = strlen(theString) / 2;
        
    for (index = 0; index < halfLength; index += 1) {
        if (theString[index] == theString[halfLength - (index + 1)]) {
            counter += 1;
        }
    }

    if (counter == halfLength) {
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char const *argv[]) {
    int numOfString, counter, index;
    counter = 0;

    scanf("%d", &numOfString);

    char words[numOfString][16];

    for (index = 0; index < numOfString; index += 1) {
        scanf(" %s", words[index]);

        counter += isPalindrom(words[index]);
    }

    if (counter == numOfString) {
        printf("valid\n");
    } else {
        printf("tidak valid\n");
    }

    return 0;
}