#include <stdio.h>

int getPoint(char toBeChecked[], char checkedWith[], int n) {
    int index, point = 0;

    for (index = 0; index < n; index += 1) {
        if (toBeChecked[index] != ' ') {
            if (toBeChecked[index] == checkedWith[index]) {
                point += 2;
            } else {
                point -= 1;
            }
        }
    }

    return point;
}

int main(int argc, char const *argv[]) {
    int numOfAnswer, index;

    scanf("%d", &numOfAnswer);

    char anwer[numOfAnswer], source[numOfAnswer];

    for (index = 0; index < numOfAnswer; index += 1) {
        scanf(" %c", &anwer[index]);
    }
    for (index = 0; index < numOfAnswer; index += 1) {
        scanf(" %c", &source[index]);
    }

    int result = getPoint(anwer, source, numOfAnswer);

    if (getPoint(anwer, source, numOfAnswer) > 80) {
        printf("A\n");
    } else if (result >= 50) {
        printf("B\n");
    } else {
        printf("remedial\n");
    }

    return 0;
}