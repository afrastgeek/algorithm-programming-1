#include <stdio.h>

int main()
{
	// declare input variable
	int tahun;
	int bulan;
	int tanggal;

	// Set prompt for user
	printf("Silahkan masukkan tahun, bulan, dan tanggal berturut-turut.\n");

	// Input
	printf("Tahun: \n");
		scanf("%d",&tahun);
	printf("Bulan: \n");
		scanf("%d",&bulan);
	printf("Tanggal: \n");
		scanf("%d",&tanggal);

	// Output
	printf("%d-%d-%d\n",tanggal,bulan,tahun);

	return 0;
}