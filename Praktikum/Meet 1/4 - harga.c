#include <stdio.h>

int main()
{
	//declare variable
	float harga;
	float diskon;
	float netto;

	// Prompt to user
	printf("Penghitung harga diskon.\n");

	// Input
	printf("Harga awal:\n");
		scanf("%f",&harga);
	printf("Diskon dalam persen:\n");
		scanf("%f",&diskon);

	// Calculation
	// diskon = diskon / 100;
	// netto = harga - (harga * diskon);
	diskon = harga * diskon / 100;
	netto = harga - diskon;

	// Output 
	printf("Harga Akhir: %0.2f\n",netto);

	return 0;
}