#include <stdio.h>

typedef struct {
	int harga;
	int banyak;
} makanan;

int main()
{
	makanan nasi_goreng, capcay;

	nasi_goreng.harga=10000;
	capcay.harga=4000;

	printf("Masukkan jumlah nasi goreng :\n");
	scanf("%d", &nasi_goreng.banyak);
	printf("Masukkan jumlah capcay :\n");
	scanf("%d", &capcay.banyak);

	printf("Total harga nasi goreng : %d\n", nasi_goreng.harga*nasi_goreng.banyak );

	printf("Total harga capcay : %d\n", capcay.harga*capcay.banyak );

	return 0;
}