#include <stdio.h>

int main()
{
	int nim, umur, submenu;
	char gender, menu;

	scanf("%d %d %c", &nim, &umur, &gender);
	scanf("%c %d", &menu, &submenu);

	printf("%d - ", nim);

	switch (menu) {
		case 'A': {
			switch (submenu) {
				case 1 :
					printf("Lotek");
				break;
				case 2 :
					printf("Steak");
				break;
				default:
					printf("Tidak ada menu");
				break;
			}
		break;
		}
		case 'B': {
			switch (submenu) {
				case 1 :
					printf("Gehu");
				break;
				case 2 :
					printf("Bala-Bala");
				break;
				case 3 :
					printf("Comro");
				break;
				case 4 :
					printf("Beng-Beng");
				break;
				default:
					printf("Tidak ada menu");
				break;
			}
		break;
		}
	}

	printf(" - %c\n", gender);

	return 0;
}