#include <stdio.h>

int main()
{
	char pilih; // deklarasi variabel
	//panduan untuk user
	printf("Masukan pilihan Anda:\n");
	printf("a) Program 1 \n");
	printf("b) Program 2 \n");

	//masukan
	scanf("%c", &pilih);
	//proses pemilihan statement dengan switch
	switch (pilih) {
		case 'a':
			printf("Run program 1\n");
		break;

		case 'b': {	//pake {} karena > 1 statement
			printf("Run program 2\n");
			printf("Please Wait\n");
		break;
		}
		
		default:
			printf("Oppps! inputan anda salah\n");
		break;
	}

	return 0;
}