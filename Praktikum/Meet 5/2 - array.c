#include <stdio.h>

int main()
{
	int n;
	scanf("%d", &n);

	int apaaja[n];
	int i;

	for (i = 0; i < n; ++i)
	{
		scanf("%d", &apaaja[i]);
	}

	for (i = 0; i < n; ++i)
	{
		printf("%d ", apaaja[i]);
	}

	return 0;
}