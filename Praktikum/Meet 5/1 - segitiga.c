#include <stdio.h>

int main()
{
	int ketebalan;
	int baris, kolom;

	scanf("%d", &ketebalan);

	for (baris = 0; baris <= ketebalan; ++baris)
	{
		for (kolom = 0; kolom < baris; ++kolom)
		{
			printf("*");
		}
		printf("\n");
	}

	return 0;
}