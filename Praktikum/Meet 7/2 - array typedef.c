#include <stdio.h>

typedef struct{
    float pembilang;
    float penyebut;
    float hasil;
} bungkus_angka; // deklarasi bungkusan

int main()
{
    int i,n; // deklarasi

    printf("Masukkan banyaknya bilangan: ");
    scanf("%i", &n);

    bungkus_angka bilangan[n];

    for (i = 0; i < n; i++)
    {
        printf("Masukkan pembilang: ");
        scanf("%f", &bilangan[i].pembilang);
        printf("Masukkan penyebut: ");
        scanf("%f", &bilangan[i].penyebut);
    }

    for (i = 0; i < n; i++)
    {
        bilangan[i].hasil = bilangan[i].pembilang/bilangan[i].penyebut;
    }

    for (i = 0; i < n; i++)
    {
        printf("Hasil dari %.2f dibagi %.2f adalah %.2f\n", bilangan[i].pembilang, bilangan[i].penyebut, bilangan[i].hasil);
    }

    return 0;
}