#include <stdio.h>

int main()
{
    int number, sum;
    sum = 0;    

    while (number != 0) {
        scanf("%d", &number); 
        sum += number;
    }

    printf("%d\n", sum);

    return 0;
}