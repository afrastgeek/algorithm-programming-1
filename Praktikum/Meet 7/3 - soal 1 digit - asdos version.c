#include <stdio.h>

int main()
{
    int number, digit = 0;

    scanf("%d", &number);

    while (number >= 1) {
        number = number / 10;
        digit ++;
    }

    printf("%d\n", digit);

    return 0;
}