#include <stdio.h>

int main()
{
    int number, digit, status;

    digit = 0;

    scanf("%d", &number);

    while (status != 1) {
        number = number / 10;
        digit += 1;

        if (number == 0) {
            status = 1;
        }
    }

    printf("%d\n", digit);

    return 0;
}