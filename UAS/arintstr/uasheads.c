/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan UAS Alpro I, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include "uasheads.h"

/**
 * @brief find the lowest integer in the half length of the array
 * @details This function return the lowest value available in the first half of
 * the array.
 * 
 * @param numOfInt Length of the array.
 * @param number The array of integer.
 * 
 * @return Lowest value available
 */
int firstHalfLowest(int numOfInt, int number[numOfInt]) {
    int lowest, halfInt;
    halfInt = numOfInt / 2;
    lowest = number[0];

    for (index1 = 0; index1 < halfInt; index1 += 1) {
        if (number[index1] < lowest) {
            lowest = number[index1];
        }
    }

    return lowest;
}

/**
 * @brief find the lowest integer in the second half of the array length.
 * @details This function return the lowest value available in the second half
 * of the array.
 * 
 * @param numOfInt Length of the array.
 * @param number The array of integer.
 * 
 * @return Lowest value available
 */
int lastHalfLowest(int numOfInt, int number[numOfInt]) {
    int lowest, halfInt;
    halfInt = numOfInt / 2;
    lowest = number[halfInt];

    for (index1 = halfInt; index1 < numOfInt; index1 += 1) {
        if (number[index1] < lowest) {
            lowest = number[index1];
        }
    }

    return lowest;
}

/**
 * @brief Find the higher value.
 * @details Find higher value from two integer
 * 
 * @param a First integer to compare
 * @param b Second integer to compare
 * 
 * @return The higher value
 */
int findHigher(int a, int b) {
    int higher;
    if (a > b) {
        higher = a;
    } else if (b > a) {
        higher = b;
    }

    return higher;
}

/**
 * @brief Print the string at human's multiplier index of an preferred value.
 * @details This procedure will print the equivalent string located at human's
 * index of string based on preferred value. The preferred value are the highest
 * number from two known values.
 * 
 * @param numOfStr Length of the string array.
 * @param highest The preferred value to access the selected string.
 * @param words The array of string.
 */
void printModsHighest(int numOfStr, int highest, char words[numOfStr][64]) {
    for (index1 = 0; index1 < numOfStr; index1 += 1) {
        if ((index1 + 1) % highest == 0) {
            printf("%s\n", words[index1]);
        }
    }
}