/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan UAS Alpro I, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include "uasheads.h"

int main(int argc, char const *argv[]) {
    /*
        Integer Variable Declaration.
    */
    int numOfInt, numOfStr, highest, firstLowest, lastLowest;
    highest = 0;

    /*
        Prompt user to input the number of preffered array of integer.
    */
    scanf("%d", &numOfInt);

    /*
        Create the array of integer.
    */
    int number[numOfInt];

    /*
        In the loop based on numOfInt, prompt user to input the array of integer
    */
    for (index1 = 0; index1 < numOfInt; index1 += 1) {
        scanf("%d", &number[index1]);
    }

    /*
        Prompt user to input the number of preffered array of string.
    */
    scanf("%d", &numOfStr);

    /*
        Create the array of string.
    */
    char words[numOfStr][64];

    /*
        In the loop based on numOfStr, prompt user to input the array of string.
    */
    for (index1 = 0; index1 < numOfStr; index1 += 1) {
        scanf(" %s", words[index1]);
    }

    /*
        Get the lowest value from each half of "number" array of integer.
    */
    firstLowest = firstHalfLowest(numOfInt, number);
    lastLowest = lastHalfLowest(numOfInt, number);

    /*
        Get the higher value from each lowest value in each half of the array.
    */
    highest = findHigher(firstLowest, lastLowest);

    /*
        Print the highest value;
    */
    printf("%d\n", highest);

    /*
        Call procedure to print the selected string.
        Selected string are located at the multiplier of highest value.
    */
    printModsHighest(numOfStr, highest, words);

    return 0;
}