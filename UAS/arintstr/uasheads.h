/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan UAS Alpro I, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>
#include <string.h>

/*
    Declare Global Variable
*/
int index1, index2, index3;

/*
    Function Definition
*/
int firstHalfLowest(int numOfInt, int number[numOfInt]);
int lastHalfLowest(int numOfInt, int number[numOfInt]);
int findHigher(int a, int b);

/*
    Procedure Definition
*/
void printModsHighest(int numOfStr, int highest, char words[numOfStr][64]);