/*
Kang Cecep memiliki n domba garut, setiap dua ekor domba menghasilkan m ekor
anak dalam setahun. Setiap tahun Kang Cecep menjual p ekor domba dan baru bisa
dijual setelah tahun ke 2 (mulai tahun ke- 3).

Tampilkan jumlah domba Kang Cecep pada tahun ke r

n domba
n = n + m (n / 2)
if r > 2, n = n - p


Ex:
2 domba
2 anak
jual 2 domba
tahun ke 2 = 8
 */
#include <stdio.h>

int main()
{
	// declare variable
	// n = initial number
	// m = reproduction value
	// p = sell value
	// r = year
	// i = for loop
	int n, m, p, r, i;

	// prompt user to input
	scanf("%d %d %d %d", &n, &m, &p, &r);

	// calculate number of sheep with reproduction every year
	for (i = 0; i < r; ++i)
	{
		n = n + (m * (n / 2));
	}

	// calculate number of sheep with sell activity after 2nd year (3rd year)
	if (r > 2)
	{
		r = r - 2;
		for (i = 0; i < r; ++i)
		{
			n = n - p;
		}
	}

	// display number of sheep at the end of r year
	printf("%d\n", n);

	return 0;
}