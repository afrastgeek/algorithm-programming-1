#include <stdio.h>

int main()
{
	// declare variable
	unsigned int n, m, k, h, r;
	int gehu, bala, pisang, cireng;
	int total;
	int laba_gehu = 100;
	int laba_bala = 150;
	int laba_pisang = 75;
	int laba_cireng = 175;

	// input prompt
	scanf("%d",&n);
	scanf("%d",&m);
	scanf("%d",&k);
	scanf("%d",&h);
	scanf("%d",&r);

	// check condition
	if (n <= 100 && m <= 100 && k <= 100 && h <= 100)
	{
		// calculate profit per item
		gehu = laba_gehu * n;
		bala = laba_bala * m;
		pisang = laba_pisang * k;
		cireng = laba_cireng * h;

		if (r <= 1000)
		{
			// calculate final value
			total = (gehu + bala + pisang + cireng) * r;

			// print calculation
			printf("%d\n", total);
		}
	}
	else
	{
		printf("");
	}
	
	return 0;
}