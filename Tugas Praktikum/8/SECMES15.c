/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 8, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>
#include <string.h>

/*
    Main Function
 */
int main(int argc, char const *argv[])
{
    /*
        Initial Integer Variable Declaration.
        - numofstring1; number of word on the first sentence.
        - index1, index2, index3; used for loop and index accessor.
        - rotation; number of string and character rotation.
        - currentlength; length of selected string.
     */
    int numofstring1, index1, index2, index3, rotation, currentlength;

    /*
        Prompt user to input the number of word on the first sentence.
     */
    scanf("%d", &numofstring1);

    /*
        Initial Character Variable Declaration.
        - string1; the first sentence, number of word = numofstring1, maximum
        length of character each word = 64.
        - stringstore; temporary variable for string/word.
        - charstore; temporary variable for character.
     */
    char string1[numofstring1][64], stringstore[64], charstore;

    /*
        Prompt user to input the word for the first sentence based on
        numofstring1.
     */
    for (index1 = 0; index1 < numofstring1; index1 += 1)
    {
        scanf(" %s", &string1[index1]);
    }

    /*
        Another Integer Variable Declaration.
        - numofstring2; number of word on the second sentence.
     */
    int numofstring2;

    /*
        Prompt user to input the number of word on the second sentence.
     */
    scanf("%d", &numofstring2);

    /*
        Another Character Variable Declaration.
        - string2; the second sentence, number of word = numofstring2, maximum
        length of character each word = 64.
     */
    char string2[numofstring2][64];

    /*
        Prompt user to input the word for the second sentence based on
        numofstring2.
     */
    for (index1 = 0; index1 < numofstring2; index1 += 1)
    {
        scanf(" %s", &string2[index1]);
    }

    /*
        Prompt user to input the number of string and character rotation
     */
    scanf("%d", &rotation);

    /*
        Loop for shuffling the string
     */
    for (index1 = 0; index1 < rotation; index1 += 1)
    {
        /*
            Shuffle the first sentence
         */
        strcpy(stringstore, string1[0]);
        for (index2 = 0; index2 < numofstring1 -1; index2 += 1)
        {
            strcpy(string1[index2], string1[index2 + 1]);
        }
        strcpy(string1[index2], string2[0]);

        /*
            Shuffle the second sentence
         */
        for (index2 = 0; index2 < numofstring2 -1; index2 += 1)
        {
            strcpy(string2[index2], string2[index2 + 1]);
        }
        strcpy(string2[index2], stringstore);
    }

    /*
        Loop for shuffling the character
     */
    for (index1 = 0; index1 < rotation; index1 += 1)
    {
        /*
            Shuffle character from word of the first sentence
         */
        charstore = string1[0][0];
        for (index2 = 0; index2 < numofstring1; index2 += 1)
        {
            currentlength = strlen(string1[index2]);
            for (index3 = 0; index3 < currentlength - 1; index3 += 1)
            {
                string1[index2][index3] = string1[index2][index3 + 1];
            }
            string1[index2][index3] = string1[index2 + 1][0];
        }
        string1[index2 - 1][index3] = charstore;

        /*
            Shuffle character from word of the second sentence
         */
        charstore = string2[0][0];
        for (index2 = 0; index2 < numofstring2; index2 += 1)
        {
            currentlength = strlen(string2[index2]);
            for (index3 = 0; index3 < currentlength - 1; index3 += 1)
            {
                string2[index2][index3] = string2[index2][index3 + 1];
            }
            string2[index2][index3] = string2[index2 + 1][0];
        }
        string2[index2 - 1][index3] = charstore;
    }

    /*
        When rotation are '0', no shuffle conducted.
        So, delete the odd character (even number on the program)
     */
    if (rotation == 0)
    {
        /*
            Loop for deleting odd character
         */
        for (index1 = 0; index1 < numofstring1; index1 += 1)
        {
            currentlength = strlen(string1[index1]) / 2;
            for (index2 = 0; index2 < currentlength; index2 += 1)
            {
                string1[index1][index2] = string1[index1][(index2 * 2) + 1];
            }
            string1[index1][currentlength] = '\0';
        }

        for (index1 = 0; index1 < numofstring2; index1 += 1)
        {
            currentlength = strlen(string2[index1]) / 2;
            for (index2 = 0; index2 < currentlength; index2 += 1)
            {
                string2[index1][index2] = string2[index1][(index2 * 2) + 1];
            }
            string2[index1][currentlength] = '\0';
        }
    }

    /*
        OUTPUT SECTION
        Print result of the operation
     */
    /* Scrambled FIRST SENTENCE */
    printf("1\n");
    for (index1 = 0; index1 < numofstring1; index1 += 1)
    {
        printf("%s", string1[index1]);

        /*
            Conditional If
            for managing spaces between string
         */
        if (index1 < numofstring1 - 1
            && ((strlen(string1[index1]) && strlen(string1[index1 + 1]) != 0)
            || (index1 > 0 && strlen(string1[index1]) == 0 && strlen(string1[index1 + 1]))))
        {
            printf(" ");
        }
    }
    /* Scrambled SECOND SENTENCE */
    printf("\n2\n");
    for (index1 = 0; index1 < numofstring2; index1 += 1)
    {
        printf("%s", string2[index1]);

        /*
            Conditional If
            for managing spaces between string
         */
        if (index1 < numofstring2 - 1
            && ((strlen(string2[index1]) && strlen(string2[index1 + 1]) != 0)
            || (index1 > 0 && strlen(string2[index1]) == 0 && strlen(string2[index1 + 1]))))
        {
            printf(" ");
        }
    }
    printf("\n");

    /*
        Return integer value of '0' as the program succeed
     */
    return 0;
}
