/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Tugas Praktikum 2, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
	Keterangan Paket:
	a. kendaraan umum = paket kelabu --->
	Biaya parkir, 2 jam pertama Rp.1500 dan Rp.500 untuk perjam selanjutnya
	b. kendaraan pribadi = paket ceria --->
	Biaya parkir, 1 jam pertama Rp.3000 dan Rp.1000 untuk perjam selanjutnya

	Khusus untuk akhir pekan (sabtu/minggu) total biaya parkir akan ditambah
	pajak sebesar 25 persen.
 */
#include <stdio.h>

	int main()
	{
		/*
			Create variable for input
			n = day of parking
			o = parking type
			p = entry hour
			q = leave hour
			r = pay
		 */
		int n = 0, p = 0, q = 0, r = 0;
		char o;
		int hour = 0, bill = 0, change = 0;

		/*
			Prompt user to input for needed variable
		 */
		scanf("%d %c %d %d %d", &n, &o, &p, &q, &r);

		/*
			Calculation
		 */
		// Calculate time of parking
		if (q <= p)
		{
			hour = 24 - p + q;
		} else
		{
			hour = q - p;
		}
		// Calculate parking bill
		if (o == 'a')
		{
			if (hour <= 2)
			{
				if (hour == 1)
				{
					bill = 1500;
				} else if (hour == 2)
				{
					bill = 3000;
				}
			} else if (hour > 2)
			{
				hour = hour - 2;
				bill = (500 * hour) + 3000;
			}
		} else if (o == 'b')
		{
			if (hour > 1)
			{
				hour = hour - 1;
				bill = (hour * 1000) + 3000;
			} else
			{
				bill = 3000;
			}
		}

		// Calculate weekend bill
		if ((n == 6) || (n == 7))
		{
			bill = (bill * 125) / 100;
		}

		// Calculate money change
		change = r - bill;

		/*
			Output
			nama hari dan paket
			uang kembalian atau tidak valid, jika biaya parkir > uang piyu
		 */
		// print day of parking
		if (n == 1)
		{
			printf("senin ");
		} else if (n == 2)
		{
			printf("selasa ");
		} else if (n == 3)
		{
			printf("rabu ");
		} else if (n == 4)
		{
			printf("kamis ");
		} else if (n == 5)
		{
			printf("jumat ");
		} else if (n == 6)
		{
			printf("sabtu ");
		} else if (n == 7)
		{
			printf("minggu ");
		}

		// print parking packages
		if (o == 'a')
		{
			printf("kelabu\n");
		} else if (o == 'b')
		{
			printf("ceria\n");
		}

		// print money change or not valid
		if (change >= 0)
		{
			printf("%d\n", change);
		} else
		{
			printf("tidak valid\n");
		}

		return 0;
	}