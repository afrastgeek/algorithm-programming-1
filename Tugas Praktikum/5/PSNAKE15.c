/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Tugas Praktikum 5, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

/*
The Worm's Turn

 
 
              _{\ _{\{\/}/}/}__
             {/{/\}{/{/\}(\}{/\} _
            {/{/\}{/{/\}(_)\}{/{/\}  _
         {\{/(\}\}{/{/\}\}{/){/\}\} /\}
        {/{/(_)/}{\{/)\}{\(_){/}/}/}/}
       _{\{/{/{\{/{/(_)/}/}/}{\(/}/}/}
      {/{/{\{\{\(/}{\{\/}/}{\}(_){\/}\}
      _{\{/{\{/(_)\}/}{/{/{/\}\})\}{/\}
     {/{/{\{\(/}{/{\{\{\/})/}{\(_)/}/}\}
      {\{\/}(_){\{\{\/}/}(_){\/}{\/}/})/}
       {/{\{\/}{/{\{\{\/}/}{\{\/}/}\}(_)
      {/{\{\/}{/){\{\{\/}/}{\{\(/}/}\}/}
       {/{\{\/}(_){\{\{\(/}/}{\(_)/}/}\}
         {/({/{\{/{\{\/}(_){\/}/}\}/}(\}
          (_){/{\/}{\{\/}/}{\{\)/}/}(_)
            {/{/{\{\/}{/{\{\{\(_)/}
             {/{\{\{\/}/}{\{\\}/}
              {){/ {\/}{\/} \}\}
              (_)  \.-'.-/
          __...--- |'-.-'| --...__
   _...--"   .-'   |'-.-'|  ' -.  ""--..__
 -"    ' .  . '    |.'-._| '  . .  '   jro
 .  '-  '    .--'  | '-.'|    .  '  . '
          ' ..     |'-_.-|
  .  '  .       _.-|-._ -|-._  .  '  .
              .'   |'- .-|   '.
  ..-'   ' .  '.   `-._.-´   .'  '  - .
   .-' '        '-._______.-'     '  .
        .      ~,
    .       .   |\   .    ' '-.
    ___________/  \____________
   /  Why is it, when you want \
  |  something, it is so much   |
  |       work to get it?       |
   \___________________________/
 */

/*
	Format Masukan :
	x y , koordinat awal snake
	n , banyaknya instruksi
	m , banyaknya langkah yang akan dilakukan 
	k l, k adalah arah ( H / V ) l adalah banyaknya step tiap satuan kartesius
	fx fy, koordinat makanan.
 */

int main()
{
	/*
		Bulk definition of integer variable
		- base coordinate (x,y) = [0,1]
		- food coordinate (x,y) = [0,1]
		- number of instruction
		- number of step
		- number of movement
		- count of score with initialization on 0
		- index and another index
	 */
	int base_coordinate[2], food_coordinate[2], instruction, step, move, score = 0, index, anotherindex;

	/*
		char variable of orientation.
		will be filled with Horizontal (H) or Vertical (V)
	 */
	char orientation;

	/*
		Prompt user to input:
		- base x coordinate
		- base y coordinate
		- number of instruction
	 */
	scanf("%d %d %d", &base_coordinate[0], &base_coordinate[1], &instruction);
	
	/*
		Create "food" integer variable based from number of instruction
		This variable will be used to tag the food.
		Value of this variable will be printed if the food is eaten.
	 */
	int food[instruction];

	/*
		Create loop based from number of instruction
	 */
	for (index = 0; index < instruction; index += 1)
	{
		/*
			Prompt user to input the number of step.
		 */
		scanf("%d", &step);

		/*
			Create loop based from number of step
		 */
		for (anotherindex = 0; anotherindex < step; anotherindex += 1)
		{
			/*
				Prompt user to input orientation and movement
				Ex: H 5, H -5, V 1, V -1.
			 */
			/*
				AN HOUR WASTED FOR FIGURING OUT SPACE BEFORE %C
				AN HOUR WASTED FOR FIGURING OUT SPACE BEFORE %C
				AN HOUR WASTED FOR FIGURING OUT SPACE BEFORE %C
				AN HOUR WASTED FOR FIGURING OUT SPACE BEFORE %C
				AN HOUR WASTED FOR FIGURING OUT SPACE BEFORE %C
			 */
			scanf(" %c %d", &orientation, &move);

			/*
				Check whether the orientation is Horizontal or Vertical
				Then calculate the next base coordinate with sum of previous base coordinate and move.

				base_coordinate[0] is x or Horizontal
				base_coordinate[1] is y or Vertical
			 */
			if ((orientation == 'h') || (orientation == 'H')) {
				base_coordinate[0] = base_coordinate[0] + move;
			} else if ((orientation == 'v') || (orientation == 'V')) {
				base_coordinate[1] = base_coordinate[1] + move;
			}
		}

		/*
			Prompt user to input the coordinate of food
			[0] for x or Horizontal
			[1] for y or Vertical
		 */
		scanf("%d %d", &food_coordinate[0], &food_coordinate[1]);

		/*
			Check, for eaten food.
			Food is eaten if CALCULATED base coordinate is equal with food coordinates
		 */
		if (( base_coordinate[0] == food_coordinate[0] ) && ( base_coordinate[1] == food_coordinate[1] ))
		{
			/*
				Assign value of 1 to food counter
				a.k.a Tagging
				FOOD is EATEN
			 */
			food[index] = 1;
		}
	}

	/*
		Create loop based on number of instruction
	 */
	for (index = 0; index < instruction; index += 1)
	{
		/*
			Check if food number 'index' is tagged (equal with 1)

			in simple: if the FOOD is EATEN
		 */
		if (food[index] == 1)
		{
			/*
				If yes, print the eaten food
				makanan ke-index
			 */
			printf("makanan %d\n", index+1);

			/*
				Add increment to score if the food is eaten
			 */
			score = score + 1;
		}
	}

	/*
		Calculate the final score

		I could replace the '10' with constant, #DEFINE RULE THE WORLD!
	 */
	score = score * 10;

	/*
		Print the Final Score
	 */
	printf("%d\n", score);

	/*
		Program is Success
	 */
	return 0;
}