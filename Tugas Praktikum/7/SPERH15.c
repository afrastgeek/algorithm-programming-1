/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 7, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
/*
    Variable Declaration
 */
    /*
        Integer Variable Definition
        - index = iterator and array accessor
        - length = store length of array
        - halflength = half of the array length
        - counter = used for count the matching character
     */
    int index1, index2, index3, length1, length2, length3, halflength3, counter;

    /*
        Char variable definition
        using array of char (string)
        - string1 = first input, the target string
        - string2 = second input, the eraser of target string
        - string3 = store the result of erased target string
     */
    char string1[64], string2[64], string3[64];

/*
    Input Section
 */
    /*
        Prompt user to input the target string and eraser string
     */
    scanf(" %s", &string1);
    scanf(" %s", &string2);

    /*
        Store the length of string in the variable
     */
    length1 = strlen(string1);
    length2 = strlen(string2);

/*
    Checker Section
 */
    /*
        Initialize the index1 variable
     */
    index1 = 0;

    /*
        Create loop at the length of target string
     */
    while (string1[index1] != '\0')
    {
        /*
            Initialize the counter variable
         */
        counter = 0;

        /*
            Foreach eraser char of string, if it match the target char of
            string at the start of index1,

            count the matching char

            if it equal with length of eraser, replace the matching char with
            asterisk ('*')
         */
        for (index2 = 0; index2 < length2; index2 += 1)
        {
            if (string1[index1 + index2] == string2[index2])
            {
                counter += 1;

                if (counter == length2)
                {
                    for (index2 = 0; index2 < length2; index2 += 1)
                    {
                        string1[index1 + index2] = '*';
                    }
                }
            }
        }

        /*
            Iterator of index1 variable
         */
        index1 += 1;
    }

/*
    Copying the unique character
 */
    /*
        Initialize the index2 variable
     */
    index2 = 0;

    /*
        For each char of target string, if not asterisk ('*'), copy the
        character to string3 variable.
     */
    for (index1 = 0; index1 < length1; index1 += 1)
    {
        if (string1[index1] != '*')
        {
            string3[index2] = string1[index1];

            /*
                Iterator of index2 variable
             */
            index2 += 1;
        }
    }

    /*
        Replace the char of string3 at index2 with null character
     */
    string3[index2] = '\0';

    /*
        Store the length of string3, and the half of length of string3
     */
    length3 = strlen(string3);
    halflength3 = length3 / 2;

/*
    Output Section
 */
    /*
        Print the string3
     */
    printf("%s\n", string3);

    /*
        Create pattern of triangle with empty space in the middle
     */
    for (index1 = 0; index1 < halflength3 + 1; index1 += 1)
    {
        for (index2 = 0; index2 < index1; index2 += 1)
        {
            printf(" ");
        }
        printf("%c", string3[index1]);
        for (index2 = length3 - 2; index2 > index1 * 2; index2 -= 1)
        {
            printf(" ");
        }
        if (index1 < halflength3)
        {
            printf("%c", string3[(length3 - 1) - index1]);
        }
        printf("\n");
    }

    /*
        Create pattern of pyramid
     */
    index1 = index3 = 0;
    while (index3 < length3)
    {
        for (index2 = halflength3; index2 > index1; index2 -= 1)
        {
            printf(" ");
        }
        for (index2 = -1; index2 < index1 * 2; index2 += 1)
        {
            if (index3 < length3)
            {
                printf("%c", string3[index3]);
                index3 += 1;
            }
        }
        printf("\n");
        index1 += 1;
    }

    return 0;
}