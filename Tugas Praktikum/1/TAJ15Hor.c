/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Tugas Praktikum 1, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	// Declare variable
	int angka;

	// Variable for reversing input number
	int ribuan;
	int ratusan;
	int puluhan;
	int satuan;
	int r_ribuan;
	int r_ratusan;
	int r_puluhan;
	int r_satuan;
	int reversed;

	// Clock variable for converted reversed number
	int jam;
	int menit;
	int detik;

	// Prompt user to input number
	scanf("%d", &angka);

	// Create conditional processing
	if (angka >= 1000 && angka <= 9999)
	{
		// Calculation
		ribuan = angka / 1000;
		ratusan = (angka / 100) % 10;
		puluhan = (angka / 10) % 10;
		satuan = angka % 10;

		// Re-declare number
		r_ribuan = satuan * 1000;
		r_ratusan = puluhan * 100;
		r_puluhan = ratusan * 10;
		r_satuan = ribuan;

		// Re-grouping number in reverse order
		reversed = r_ribuan + r_ratusan + r_puluhan + r_satuan;
		
		// Conversing reversed number to clock format
		detik = reversed % 60;
		menit = (reversed / 60) % 60;
		jam = (reversed / 3600);

		// Printing Output to Screen
		printf("%d\n",reversed);
		printf("%.2d:%.2d:%.2d\n", jam, menit, detik);
	}

	return 0;
}