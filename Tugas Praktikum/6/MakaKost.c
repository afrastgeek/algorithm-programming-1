/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 6, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Include the Standard Input Output Header file, for the formatted scan and
    formatted print function.
 */
#include <stdio.h>

/*
    typedef and struct are two different things.
    Y U NO Explain this?

    Create a new structure type.
    Actually, this structure has no name.
    The structure name altered with typedef declaration.
 */
typedef struct /* Here is the place for the structure name */
{
/*
    The Structure.
    Create rate and price structure.

    rate: 1 ~ 10
    price: ~
 */
    int rate;
    int price;

/*
    Declare the name of the defined structure type with "food_properties".
    So, we can use it without "struct".
    Ex:
    food_properties var;

    Not:
    struct *struct_name* var;
 */
} food_properties;

/*
   Declare the main function.
   Which is called when program started.
   Based on ISO C 9899:1999, This function should return an int because
   we're running it on hosted environment (on top of an OS).
   see return 0; 

   The parameter are additional. void also could be used.
 */
int main(int argc, char const *argv[])
{
    /*
        Declare variable from defined structure.
        - food, array of food with rate and price.
     */
    food_properties food[64];

    /*
        Declare int variable for:
        - balance, Amount of money Ziah has.
        - result, storing selected element index.
        - temp, storing temporary array element.
            it will be used for comparing prices.
            - low = for cheap food pricing
            - high = for expensive food pricing
        - index, for looping.
        - inputindex, special index for input loop
     */
    int balance, result, templow, temphigh, index, inputindex;

    /*
        Declare char variable for:
        - food rating, assume the value are:
            - S = sedang (food rate: 1 ~ 5)
            - E = enak (food rate: 6 ~ 10)
        - food pricing
            - J = mahal
            - K = murah
     */
    char rating, pricing;

    /*
        Initialization
        - index variable with 0.
        - result variable with -1, this will be the default value.
     */
    inputindex = 0;
    result = -1;

    /*
        Prompt the First input of food rate to the user.
     */
    scanf("%d", &food[inputindex].rate);

    /*
        As long as the food rate is not "0", run the loop.
     */
    while (food[inputindex].rate != 0)
    {
        /*
            Prompt the food price with previous element index.
         */
        scanf("%d", &food[inputindex].price);

        /*
            Increment the index value.
         */
        inputindex += 1;

        /*
            Prompt the food rate input with new element index.
         */
        scanf("%d", &food[inputindex].rate);
    }

    /*
        Prompt the food rating and food pricing value.
        Expected Input:
        (E || S) (J || K)
        E J
        E K
        S J
        S K
     */
    scanf(" %c %c", &rating, &pricing);

    /*
        Prompt the amount of Ziah money
     */
    scanf("%d", &balance);



    /* 
        PROCESS SECTION
     */
    /*
        Assign value to the temporary variable.
     */
    temphigh = templow = 0;

    /*
        Create loop for finding highest price, then store it on templow
        variable.
     */
    index = 0;
    while (index <= inputindex)
    {
        if (templow <= food[index].price)
        {
            templow = food[index].price;
        }
        index += 1;
    }

    /* Re-initialize index */
    index = 0;

    /*
        Create conditional if for process based on Food Rating
        E = Enak
        S = Sedang
     */
    if (rating == 'E') /* Enak Food */
    {
        /*
            Create conditional if for process based on Foor Pricing
            J = Mahal
            K = Murah
         */
        if (pricing == 'J') /* Mahal Food */
        {
            /*
                Create loop for each array element
             */
            while (index <= inputindex)
            {
                /*
                    Conditional check.
                    For every array element, if:
                    - food price higher than temphigh,
                    - food rate higher than or same with 6 (enak),
                    - food price lower or same with balance.
                 */
                if (temphigh <= food[index].price && food[index].rate >= 6 && food[index].price <= balance)
                {
                    /* 
                        Change the temporary value
                     */
                    temphigh = food[index].price;

                    /* 
                        Tag the element index using result variable, replacing
                        default value
                    */
                    result = index;
                }

                /*
                    Increment the index.
                 */
                index += 1;
            }
        } else if (pricing == 'K') /* Murah Food */
        {
            /*
                Create loop for each array element
             */
            while (index <= inputindex)
            {
                /*
                    Conditional check.
                    For every array element, if:
                    - food price lower than templow,
                    - food rate higher than or same with 6 (enak),
                    - food price lower or same with balance.
                 */
                if (templow >= food[index].price && food[index].rate >= 6 && food[index].price <= balance)
                {
                    /* 
                        Change the temporary value
                     */
                    templow = food[index].price;

                    /* 
                        Tag the element index using result variable, replacing
                        default value
                    */
                    result = index;
                }

                /*
                    Increment the index.
                 */
                index += 1;
            }
        }
    } else if (rating == 'S') /* Sedang Food */
    {
        if (pricing == 'J') /* Mahal Food */
        {
            /*
                Create loop for each array element
             */
            while (index <= inputindex)
            {
                /*
                    Conditional check.
                    For every array element, if:
                    - food price higher than temphigh,
                    - food rate lower than 6 (sedang),
                    - food price lower or same with balance.
                 */
                if (temphigh <= food[index].price && food[index].rate < 6 && food[index].price <= balance)
                {
                    /* 
                        Change the temporary value
                     */
                    temphigh = food[index].price;

                    /* 
                        Tag the element index using result variable, replacing
                        default value
                    */
                    result = index;
                }

                /*
                    Increment the index.
                 */
                index += 1;
            }
        } else if (pricing == 'K') /* Murah Food */
        {
            /*
                Create loop for each array element
             */
            while (index <= inputindex)
            {
                /*
                    Conditional check.
                    For every array element, if:
                    - food price lower than templow,
                    - food rate lower than 6 (sedang),
                    - food price lower or same with balance.
                 */
                if (templow >= food[index].price && food[index].rate < 6 && food[index].price <= balance)
                {
                    /* 
                        Change the temporary value
                     */
                    templow = food[index].price;

                    /* 
                        Tag the element index using result variable, replacing
                        default value
                    */
                    result = index;
                }

                /*
                    Increment the index.
                 */
                index += 1;
            }
        }
    }


    /*
        Create conditional statement for printing output
        Default value of result variable used to print default output
        "Kelaparan".

        Otherwise, print the food rate and food price based on tagged element
        index with result variable.
     */
    if (result == -1)
    {
        printf("Kelaparan\n");
    } else
    {
        printf("%d\n%d\n", food[result].rate, food[result].price);
    }

    /*
        Return the success code, 0.
     */
    return 0;
}
