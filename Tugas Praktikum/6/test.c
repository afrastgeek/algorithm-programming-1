#include <stdio.h>

typedef struct {
    int a;
    int b;
}abc;

int main(int argc, char const *argv[])
{
    abc sangray[5];
    sangray[0].a = 1;
    sangray[1].a = 2;
    sangray[2].a = 3;
    sangray[3].a = 4;
    sangray[4].a = 5;
    sangray[0].b = 6;
    sangray[1].b = 7;
    sangray[2].b = 3;
    sangray[3].b = 4;
    sangray[4].b = 8;
    int i, temp;

    temp = sangray[0].a;
    // for (i = 0; i < 5; ++i)
    while (i < 5 && sangray[i].b < 6)
    {
        if (temp < sangray[i].a)
        {
            temp = sangray[i].a;
        }
    }

    printf("%d\n", temp);

    return 0;
}