/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Tugas Praktikum 4, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	/*
		Create variable for:
		- thickness
		- index and another index
	 */
	int thickness, index, anotherindex;

	/*
		Prompt user to input the number thickness
	 */
	scanf("%d", &thickness);

	/*
		First Row Group
	 */
	for (index = 0; index < thickness; index += 1)
	{
		/*
			First Triangle
		 */
		for (anotherindex = thickness; anotherindex >= index; anotherindex -= 1)
		{
			printf(" ");
		}
		/*
			Second Triangle
		 */
		for (anotherindex = 0; anotherindex <= index; ++anotherindex)
		{
			printf("**");
		}
		for (anotherindex = thickness - 1; anotherindex > index; anotherindex -= 1)
		{
			printf(" ");
		}
		for (anotherindex = 1; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = 1; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = 1; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = thickness - 1; anotherindex > index; anotherindex -= 1)
		{
			printf(" ");
		}
		for (anotherindex = 0; anotherindex <= index; ++anotherindex)
		{
			printf("**");
		}
		printf("\n");
	}

	/*
		Second Row Group
	 */
	for (index = 0; index < thickness; ++index)
	{
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex <= thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		printf("\n");
	}

	/*
		Third Row Group
	 */
	for (index = 0; index < thickness; ++index)
	{
		for (anotherindex = 0; anotherindex < index; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = thickness; anotherindex >= index; anotherindex -= 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 1; anotherindex < thickness; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = 0; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = thickness; anotherindex >= index; anotherindex -= 1)
		{
			printf("*");
		}
		printf("\n");
	}

	/*
		Fourth Row Group
	 */
	for (index = 0; index < thickness; ++index)
	{
		if (thickness == 1)
		{
			for (anotherindex = 0; anotherindex <= thickness + 1 ; anotherindex += 1)
			{
				printf(" ");
			}
		} else {
			for (anotherindex = 0; anotherindex <= thickness ; anotherindex += 1)
			{
				printf(" ");
			}
			for (anotherindex = 0; anotherindex <= thickness ; anotherindex += 1)
			{
				printf(" ");
			}	
		}
		for (anotherindex = 0; anotherindex < index; anotherindex += 1)
		{
			printf(" ");
		}
		for (anotherindex = thickness; anotherindex > index; anotherindex -= 1)
		{
			printf("*");
		}
		for (anotherindex = 3; anotherindex < thickness; anotherindex += 1)
		{
			printf("*");
		}
		for (anotherindex = thickness; anotherindex > index; anotherindex -= 1)
		{
			printf("*");
		}
		printf("\n");
	}

	return 0;
}