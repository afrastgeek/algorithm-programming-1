/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Tugas Praktikum 3, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

/* spesifikasi muatan waktu */
typedef struct {
	int hour, minute, second;
}time;

int main()
{
	/* create variable for input */ 
	int kendaraan, hari;
	/* create variable for calculation */
	time mobil, bus, pesawat, departure, arrival;

	/* prompt user to input */
	scanf("%d", &kendaraan);
	scanf("%d", &hari);
	scanf("%d %d %d", &departure.hour, &departure.minute, &departure.second);

	/* initialize transportation travel time */
	mobil.hour = 7;
	mobil.minute = 20;
	mobil.second = 33;

	bus.hour = 9;
	bus.minute = 10;
	bus.second = 15;

	pesawat.hour = 2;
	pesawat.minute = 40;
	pesawat.second = 10;

	/* switch case for transportation type
		calculate travel time for departure time
		print output to user */
	switch(kendaraan) {
		case 1 : {
			/* calculate for mobil travel time */
			arrival.second = (departure.second + mobil.second);
			
			if (arrival.second > 59)
			{
				arrival.minute = (departure.minute + mobil.minute) + (arrival.second / 60);
				arrival.second = arrival.second % 60;
			}
			else
			{
				arrival.minute = ((departure.minute + mobil.minute));	
			}

			if (arrival.minute > 59)
			{
				arrival.hour = (departure.hour + mobil.hour) + (arrival.minute / 60);
				arrival.minute = arrival.minute % 60;
			}
			else
			{
				arrival.hour = ((departure.hour + mobil.hour));	
			}

			if (arrival.hour > 24)
			{
				hari = hari + (arrival.hour / 24);
				arrival.hour = arrival.hour % 24;
			}

			if (hari > 7)
			{
				hari = hari % 7;
			}

			/* print output to user */
			printf("mobil\n%d\n%d %d %.2d\n", hari, arrival.hour, arrival.minute, arrival.second);
		}
		break;
		case 2 : {
			/* calculate for bus travel time */
			arrival.second = (departure.second + bus.second);
			
			if (arrival.second > 59)
			{
				arrival.minute = (departure.minute + bus.minute) + (arrival.second / 60);
				arrival.second = arrival.second % 60;
			}
			else
			{
				arrival.minute = ((departure.minute + bus.minute));	
			}

			if (arrival.minute > 59)
			{
				arrival.hour = (departure.hour + bus.hour) + (arrival.minute / 60);
				arrival.minute = arrival.minute % 60;
			}
			else
			{
				arrival.hour = ((departure.hour + bus.hour));	
			}

			if (arrival.hour > 24)
			{
				hari = hari + (arrival.hour / 24);
				arrival.hour = arrival.hour % 24;
			}

			if (hari > 7)
			{
				hari = hari % 7;
			}

			/* print output to user */
			printf("bus\n%d\n%d %d %.2d\n", hari, arrival.hour, arrival.minute, arrival.second);
		}
		break;
		case 3 : {
			/* calculate for pesawat travel time */
			arrival.second = (departure.second + pesawat.second);
			
			if (arrival.second > 59)
			{
				arrival.minute = (departure.minute + pesawat.minute) + (arrival.second / 60);
				arrival.second = arrival.second % 60;
			}
			else
			{
				arrival.minute = ((departure.minute + pesawat.minute));	
			}

			if (arrival.minute > 59)
			{
				arrival.hour = (departure.hour + pesawat.hour) + (arrival.minute / 60);
				arrival.minute = arrival.minute % 60;
			}
			else
			{
				arrival.hour = ((departure.hour + pesawat.hour));	
			}

			if (arrival.hour > 24)
			{
				hari = hari + (arrival.hour / 24);
				arrival.hour = arrival.hour % 24;
			}

			if (hari > 7)
			{
				hari = hari % 7;
			}

			/* print output to user */
			printf("pesawat\n%d\n%d %d %.2d\n", hari, arrival.hour, arrival.minute, arrival.second);
		}
		break;
		default :
			/* default value */
			printf("pilihan tidak ada\n");
		break;
	}

	return 0;
}