/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Call Default C Language Library.
    stdio.h     Used for scanf and printf function.
    string.h    Used to enable string management support.
*/
#include <stdio.h>
#include <string.h>

/*
    Global Variable declaration.
*/
int index1, index2, index3, currentLength;

/*
    Procedure Definition
*/
void binRequest(int r, int n, char s[n][64]);
void hepiFeature(int n, char s[n][64]);
void displaySecret(int r, int n, char s[n][64]);
