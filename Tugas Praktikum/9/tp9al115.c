/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Call "tp9heads" Header File
 */
#include "tp9heads.h"

/**
 * @brief Program's Main Function.
 * @details This is the default function called by program when executed.
 * 
 * @return Return the value of zero when the program succeed.
 */
int main(int argc, char const *argv[]) {

    /*
        Variable Declaration
     */
    int numOfString, numOfReplace;
    char string[128][64];

    /*
        Create loop for string input.
        The string input prompt end when user input "end".
     */
    numOfString = -1;
    do {
        numOfString += 1;
        scanf(" %s", string[numOfString]);
        index1 = 0;
    } while(strcmp(string[numOfString], "end") != 0);

    /*
        Set maximum number of string to be ten.
     */
    if (numOfString > 10) {
        numOfString = 10;
    }

    /*
        Prompt user to input number of words to be replaced
     */
    scanf("%d", &numOfReplace);

    /*
        Call "binRequest" procedure with argument of:
        - numOfReplace (integer)
        - numOfString (integer)
        - string (double dimension char array)
     */
    binRequest(numOfReplace, numOfString, string);

    /*
        When the value of numOfReplace are zero, execute Hepi's special feature.
     */
    if (numOfReplace == 0) {
        hepiFeature(numOfString, string);
    }

    /*
        Call "secret" procedure to print the scrambled input.
     */
    displaySecret(numOfReplace, numOfString, string);

    return 0;
}
