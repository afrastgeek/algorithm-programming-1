# tp9al115 Guide
*December 12, 2015 5:10 AM.*

*Any question? Feel free to [contact](http://about.me/ammarfr) me.*

*Contribution are welcome.*


[TOC]

## Overview
Di panduan ini, bakal saya jelasin langkah-langkah yang saya pake buat ngerjain
tp9.

## Declare Variable
Jangan lupa declare dulu variabel yang diperlukan. Misal:

```c
int a, b, c[];
char x, y[a], z[b][a];
```

## Input Blocks
Pertama bikin dulu bagian input.


Format masukan :

`string`, berhenti ketika ada input end.

`n`, banyaknya karakter yang akan disamarkan.

`x y`, `x` = karakter yang akan disamarkan, `y` = karakter penyamar.

### String Input
Input buat string bakal berhenti kalo kita nulis "end". Caranya pake while ato
do while.

Contoh, dari praktikum ke-7. Input berhenti kalo angka yang di-inputnya 0.

```c
scanf("%d", &angka);

while (angka != 0) {
    scanf("%d", &angka); 
}
```

```c
do {
    scanf("%d", &angka);
} while (angka != 0);
```

Di soal, disebutin **input gaboleh dibatasin**. Tapi cuma 10 string pertama yang
bakal di proses.

Disini saya pake counter buat ngitung input (`jumlahString++`) **di dalam
while/do while nya**. Tapi kalo counter lebih dari 10, set counternya jadi 10.

```c
if (jumlahString > 10) {
    jumlahString = 10;
}
```

### String Operation Input
Di bagian input yang ini, kita input dulu berapa huruf yang mau diganti 
(disamarkan). Kalo di soal, pake variabel `n`.

Kemudian buat loop sebanyak input sebelumnya (`n`) untuk minta user memasukkan 
karakter yang akan diganti (`x`) dan karakter pengganti (`y`).

Contoh loop, bisa for atau while.
```c
for (i = 0; i < n; ++i) {
    scanf(" %c %c", &diganti, &pengganti);
}
```
```c
i = 0;
while (i < n) {
	scanf(" %c %c", &diganti, &pengganti);
    i++;
}
```

## Process Blocks
Di bagian ini, mulai mengubah struktur string berdasarkan input.
Ada dua case,
1. Ketika input `n`-nya bukan 0, dan
2. Ketika input `n`-nya 0.

### Case 1, `n` isn't zero.
Pada kasus ini, kita mengganti huruf di semua string dengan huruf yang di input.

Proses dilakukan setelah setiap input karakter yang diganti (`x`) dan karakter 
pengganti (`y`) di-tik oleh user. Jadi, proses kita lakukan didalam loop.

### Case 2, `n` is zero.
Pada kasus ini, kita "menggeser" huruf vokal ke belakang dan menampilkan
outputnya terbalik.

Detail fitur rahasia : 
> ada perang di dapur (4 string)

> uda pareng da dipar (huruf vokal di dorong ke belakang)

> adu gnerap ad rapid (urutan huruf dibalik)

> rapid ad gnerap adu (urutan kata dibalik)

#### Menggeser Vokal
Untuk menggeser vokal, ada 2 cara.
- **Tidak Langsung**: Simpan dulu setiap huruf vokal di array, kemudian salin
vokal dari array barusan ke string sebelumnya dengan index awal yang di-ubah.
- **Langsung**: Cari dulu index huruf vokal yang pertama. Dengan menggunakan dua
temporary char, salin huruf vokal pertama ke temp1. Kemudian salin huruf vokal 
selanjutnya ke temp2. Ganti huruf vokal "selanjutnya" dengan temp1. Ganti temp1 
dengan temp2. Terakhir, ganti huruf vokal pertama dengan temp1.

#### Membalik Urutan Huruf dan Kata
Ada 2 cara untuk membalik urutan huruf.
- Dibalik di arraynya, pake loop. Balik indeks dan pake bantuan temp, ganti 
huruf pake =, balik string pake strcpy.
- Dibalik nge-print nya, pake loop. Balik indeks biar nge-printnya dari belakang
.

Saya prefer yang kedua.

## Print Block
Tampilkan string yang udah di-ubah sebelumnya. Jika `n == 0`, jangan lupa balik 
print nya.

## Procedure
Deklarasi prosedur menggunakan `void`. Sintaksnya:
```c
void nama_prosedur(argumen) {
	/* kode program */
}
```

Untuk membuat prosedur, cukup menyalin kode yang akan dijadikan prosedur ke 
dalam blok prosedur.
[Contoh Prosedur](https://gitlab.com/afrastgeek/algorithm-programming-1/blob/master/Meet/12/1%20-%20prosedur%20pola.c)

## Custom Header File (File Separation)
Lihat: [Contoh](https://gitlab.com/afrastgeek/algorithm-programming-1/tree/master/Meet/12/2%20-%20file%20separation)