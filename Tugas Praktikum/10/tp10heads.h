/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Call Default C Language Library.
    stdio.h     Used for scanf and printf function.
    string.h    Used to enable string management support.
 */
#include <stdio.h>
#include <string.h>

/*
    Global Variable declaration.
 */
int index1, index2, index3, i, j, k;

/*
    Card Properties. Every card have maximum length of two character identifier.
 */
typedef struct cardProperties {
    char cards[4];
} cardProperties;

/*
    Function Definition
 */
int cardRate(char c[2]);
int isEmpty(int a, int b, cardProperties c[a][3]);
