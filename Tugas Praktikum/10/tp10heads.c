/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Call "tp10heads" Header File
 */
#include "tp10heads.h"

/**
 * @brief Calculate rating of a card.
 * @details This function will calculate a card rate based on ideal card stacks.
 * 
 * @param c Target card to calculate.
 * @return Rate of the card.
 */
int cardRate(char c[4]) {
    /*
        Integer variable declaration
    */
    int rate = 1;

    /*
        Ideal Cards Stacks Declaration
     */
    char idealCard[17][4] = {"2", "3", "4", "5", "6", "7", "8", "9", "10",
                             "J", "j", "Q", "q", "K", "k", "A", "a"};

    /*
        Find equivalent card rate.
        Numeric card's rate are it's own number.
        Jack = 11;
        Queen = 12;
        King = 13;
        As = 14;

    */
    k = 0;
    while (strlen(c) && k < 17) {
        if (strcmp(c, idealCard[k]) == 0
            && k <= 7) {
            rate = k + 2;
        } else if (strcmp(c, idealCard[8]) == 0) {
            rate = 10;
        } else if (strcmp(c, idealCard[k]) == 0
            && (k == 9 || k == 10)) {
            rate = 11;
        } else if (strcmp(c, idealCard[k]) == 0
            && (k == 11 || k == 12)) {
            rate = 12;
        } else if (strcmp(c, idealCard[k]) == 0
            && (k == 13 || k == 14)) {
            rate = 13;
        } else if (strcmp(c, idealCard[k]) == 0
            && (k == 15 || k == 16)) {
            rate = 14;
        } 
        k += 1;
    }

    return rate;
}

/**
 * @brief Is player hands freed?
 * @details This function will check the current condition of players hand. When
 * player still hold a card return 0, otherwise return 1.
 * 
 * @param a Number of player.
 * @param b Number of cards drawed.
 * @param c Cards hold in players hand.
 * @return The condition of players hand.
 */
int isEmpty(int a, int b, cardProperties c[a][3]) {
    /*
        Integer variable declaration.
    */
    int result = 1;

    /*
        Select each card then change the result to be 0 when cards exist.
    */
    for (i = 0; i < a; i += 1) {
        for (j = 0; j < b; j += 1) {
            if (strlen(c[i][j].cards)) {
                result = 0;
            }
        }
    }

    return result;
}