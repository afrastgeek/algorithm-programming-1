/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Tugas Praktikum 9, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
/*
    Call "tp10heads" Header File
 */
#include "tp10heads.h"

/**
 * @brief Program's Main Function.
 * @details This is the default function called by program when executed.
 * 
 * @return Return the value of zero when the program succeed.
 */
int main(int argc, char const *argv[]) {
    /*
        Integer variable declaration
    */
    int cardDraw, numOfPlayer, highest, who, which, currentRate, topCard;

    /*
        Integer variable initialization
    */
    numOfPlayer = 4;
    currentRate = 0;

    /*
        Prompt user to input the number of cards to be drawed.
    */
    scanf("%d", &cardDraw);

    /*
        Declaration of cards in number of maximum cards drawed and player number
    */
    cardProperties draw[numOfPlayer][3];

    /*
        Prompt user to input the cards in order of each player.
    */
    for (index1 = 0; index1 < numOfPlayer; index1 += 1) {
        for (index2 = 0; index2 < cardDraw; index2 += 1) {
            scanf(" %s", draw[index1][index2].cards);
            // printf("i:%d i2:%d = %s %d\n", index1, index2, draw[index1][index2].cards, cardRate(draw[index1][index2].cards));
        }
    }
    
    /*
        Print the highest card while players still hold cards.
    */
    int index = 0;
    while(isEmpty(numOfPlayer, cardDraw, draw) != 1) {
        /*
            Highest card initialization.
        */
        highest = 0;

        /*
            Find card with the highest rate in the first row of array.
        */
        for (index1 = 0; index1 < numOfPlayer; index1 += 1) {
            topCard = -1;

            /*
                Select card available at the top position.
            */
            index2 = 0;
            while (topCard == -1) {
                if (strlen(draw[index1][index2].cards)) {
                    topCard = index2;
                }
                index2 += 1;
            }

            /*
                Compare each cards.
            */
            currentRate = cardRate(draw[index1][topCard].cards);
            if (currentRate > highest) {
                who = index1;
                which = topCard;
                highest = currentRate;
            }
            // printf("i: %d i2: %d %s cR: %d\n", index1, index2, draw[index1][index2].cards, currentRate);
        }
        // printf("the highest: %s(%d) %d (%d %d)\n", draw[who][which].cards, strlen(draw[who][which].cards), cardRate(draw[who][which].cards), who, which);

        /*
            Print the highest card, while it's a cards!
        */
        if (highest != 1) {
            printf("%s\n", draw[who][which].cards);
        }
            // printf("i:%d i2:%d = %s %d\n", who, which, draw[who][which].cards, cardRate(draw[who][which].cards));

        /*
            Nullify the already printed highest card.
            (Remove it from the player hands)
        */
        draw[who][which].cards[0] = '\0';

        index += 1;
    }

    return 0;
}
