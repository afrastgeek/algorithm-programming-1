// Program sederhana untuk penjumlahan
// M Ammar FR
// 14 Sep 2015
#include <stdio.h>

// buat fungsi main
int main()
{
	// membuat variabel untuk input
	float angka_float = 2.55;
	int angka1;

	angka1 = angka_float;

	printf("%.2f %d\n", angka_float, angka1);

	return 0;
}