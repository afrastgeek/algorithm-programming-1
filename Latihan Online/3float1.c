#include <stdio.h>

int main()
{
	// declare variable
	float angka[6];
	int angka_depan;
	int angka_belakang;
	int i;
	int count = 0;

	// create loop for 6 input
	for(i = 0; i < 6; ++i)
	{
		// input
		scanf("%f",&angka[i]);
		// calculate number behind the point of input
		angka_depan = angka[i];
		angka_belakang = (angka[i] - angka_depan) * 10;

		// count whether the number is could be divided by 3
		if ( (angka_belakang % 3) == 0 && angka_belakang != 0)
		{
			count++;
		}
	}

	// conditional
	// if have 3 or more input with number behind point divided by 3
	if (count >= 3)
	{
		printf("valid\n");
	}
	else
	{
		printf("tidak valid\n");
	}

	return 0;
}