#include <stdio.h>

int main()
{
	// declare variable
	int angka[6];
	int ribuan;
	int i;
	int count = 0;

	// create loop for 6 input
	for(i = 0; i < 6; ++i)
	{
		// input
		scanf("%d",&angka[i]);
		// check whether input is a thousands
		ribuan = angka[i] / 1000;

		// count when the number is thousands
		if (ribuan >= 1)
		{
			count++;
		}
	}

	// conditional
	// if input have 3 or more input with thousands
	if (count >= 3)
	{
		printf("ribuan 3 atau lebih\n");
	}
	else
	{
		printf("tidak valid\n");
	}

	return 0;
}