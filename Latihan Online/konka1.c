#include <stdio.h>

int main()
{
	// create variable for input;
	char kar1, kar2, kar3, kar4, kar5, kar6;
	
	// create variable for storing value;
	int a=0, b=0, c=0;
	
	// prompt user to input. added space for "enter" tollerant.	
	scanf("%c %c %c %c %c %c", &kar1, &kar2, &kar3, &kar4, &kar5, &kar6);
	
	// check whether input follow the pattern of number-consonant or vice versa
	if (((kar1 >= 'a' && kar1 <= 'z') || (kar1 >= 'A' && kar1 <= 'Z')) && (kar2 >= '0' && kar2 <= '9')) {
		a = 1;
	} else if ((kar1 >= '0' && kar1 <= '9') && ((kar2 >= 'a' && kar2 <= 'z') || (kar2 >= 'A' && kar2 <= 'Z'))) {
		a = 1;
	}

	if (((kar3 >= 'a' && kar3 <= 'z') || (kar3 >= 'A' && kar3 <= 'Z')) && (kar4 >= '0' && kar4 <= '9')) {
		b = 1;
	} else if ((kar3 >= '0' && kar3 <= '9') && ((kar4 >= 'a' && kar4 <= 'z') || (kar4 >= 'A' && kar4 <= 'Z'))) {
		b = 1;
	}

	if (((kar5 >= 'a' && kar5 <= 'z') || (kar5 >= 'A' && kar5 <= 'Z')) && (kar6 >= '0' && kar6 <= '9')) {
		c = 1;
	} else if ((kar5 >= '0' && kar5 <= '9') && ((kar6 >= 'a' && kar6 <= 'z') || (kar6 >= 'A' && kar6 <= 'Z'))) {
		c = 1;
	}

	// print output based on pattern check result
	if (a == 1 && b == 1 && c == 1) {
		printf("kombinasi valid\n");
	} else {
		printf("kombinasi tidak valid\n");
	};
	
	return 0;
}