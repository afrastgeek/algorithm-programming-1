/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Kuis 1 Alpro I, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	/* create variable for input */
	char a, b, c;

	/* prompt user to input */
	scanf("%c %c %c", &a, &b, &c);

	/* check apakah vokal atau konsonan
		jika vokal = 2
		jika konsonan = 1
	 */
	if ((a != 'a') && (a != 'i') && (a != 'u') && (a != 'e') && (a != 'o'))
	{
		a = '1';
	} else if ((a == 'a') || (a == 'i') || (a == 'u') || (a == 'e') || (a == 'o')) {
		a = '2';
	}

	if ((b != 'a') && (b != 'i') && (b != 'u') && (b != 'e') && (b != 'o'))
	{
		b = '1';
	} else if ((b == 'a') || (b == 'i') || (b == 'u') || (b == 'e') || (b == 'o')) {
		b = '2';
	}

	if ((c != 'a') && (c != 'i') && (c != 'u') && (c != 'e') && (c != 'o'))
	{
		c = '1';
	} else if ((c == 'a') || (c == 'i') || (c == 'u') || (c == 'e') || (c == 'o')) {
		c = '2';
	}

	/* print output */
	printf("%c %c %c\n", a, b, c);

	return 0;
}