/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan Kuis 1 Alpro I, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	/* create variable for input */
	int a, b, c, d;

	/* prompt user to input */
	scanf("%d %d %d %d", &a, &b, &c, &d);

	/* check whether an input is could be divided by another two input or not */
	if (((a % b == 0) && (a % c == 0)) || ((a % b == 0) && (a % d == 0)) || ((a % c == 0) && (a % d == 0)))
	{
		/* print output if criteria is fullfilled */
		printf("memenuhi\n");
	} else if (((b % a == 0) && (b % c == 0)) || ((b % a == 0) && (b % d == 0)) || ((b % c == 0) && (b % d == 0)))
	{
		printf("memenuhi\n");
	} else if (((c % a == 0) && (c % b == 0)) || ((c % a == 0) && (c % d == 0)) || ((c % b == 0) && (c % d == 0)))
	{
		printf("memenuhi\n");
	} else if (((d % a == 0) && (d % b == 0)) || ((d % a == 0) && (d % c == 0)) || ((d % b == 0) && (c % d == 0)))
	{
		printf("memenuhi\n");
	} else
	{
		/* print output if criteria is not fullfilled */
		printf("tidak memenuhi\n");
	}

	return 0;
}