/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Kuis 2 Alpro I, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>
#include <string.h>

/*
    Main Function
 */
int main(int argc, char const *argv[])
{
    /*
        Define Integer Variable
        - numofstring
        - index, jindex, kindex
     */
    int numofstring, index, jindex, kindex;

    /*
        Prompt user to input the number of string
     */
    scanf("%d", &numofstring);

    /*
        Define array of string based on numofstring with length of 64 for each string.
     */
    char string[numofstring][64];

    /*
        Prompt user to input the string as much as numofstring
     */
    for (index = 0; index < numofstring; index += 1)
    {
        scanf(" %s", &string[index]);
    }

    /*
        Print the pattern of string

        Loop incremented at 3 beacause a row of pattern build from 3 string.
     */
    for (index = 0; index < numofstring; index += 3)
    {
        /*
            Print the first string, in normal form
         */
        printf("%s\n", string[index]);

        /*
            Check, whether the first string are larger or equal with the second string
         */
        if (strlen(string[index + 1]) >= strlen(string[index + 2]))
        {
            /*
                Create loop with maximum loop are the length of the second string.
             */
            for (jindex = 0; jindex < strlen(string[index + 1]); jindex += 1)
            {
                /*
                    Print the char of second string
                 */
                printf("%c", string[index + 1][jindex]);

                /*
                    If the index still lower than length of the third string, print space and the third string
                 */
                if (jindex < strlen(string[index + 2]))
                {
                    /*
                        Print the space to locate the third atring at the end of first string
                     */
                    for (kindex = 2; kindex < strlen(string[index]); kindex += 1)
                    {
                        printf(" ");
                    }
                    
                    /*
                        Print the char of third string
                     */
                    printf("%c", string[index + 2][jindex]);
                }

                /*
                    Print enter character
                 */
                printf("\n");
            }
        } else
        {
            /*
                Create loop with maximum loop are the length of the third string.
             */
            for (jindex = 0; jindex < strlen(string[index + 2]); jindex += 1)
            {
                /*
                    If the index still lower than length of the second string, print space and the third string
                    Otherwise, only print the space
                 */
                if (jindex < strlen(string[index + 1]))
                {
                    /*
                        Print the char of second string
                     */
                    printf("%c", string[index + 1][jindex]);

                    /*
                        Print the space to locate the third atring at the end of first string
                     */
                    for (kindex = 2; kindex < strlen(string[index]); kindex += 1)
                    {
                        printf(" ");
                    }
                } else
                {
                    /*
                        Print the space to locate the third atring at the end of first string
                     */
                    for (kindex = 1; kindex < strlen(string[index]); kindex += 1)
                    {
                        printf(" ");
                    }
                }
                printf("%c", string[index + 2][jindex]);

                /*
                    Print enter character
                 */
                printf("\n");
            }
        }
    }
    printf("\n");    
        
    /*
        Return integer of 0 to the program as the program succeed
     */
    return 0;
}