/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Kuis 2 Alpro I, jika saya melakukan kecurangan maka Allah/
 *  Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */

/*
    TODO:
    repair bug when (numofstring % 3 != 0)
 */
#include <stdio.h>
#include <string.h>

/*
    Build the struct counter and define it with the name counter
    Variable:
    - vocal
    - consonant
 */
typedef struct counter {
    int vocal;
    int consonant;
} counter;

/*
    Main Function
 */
int main(int argc, char const *argv[])
{
    /*
        Variable Definition
        Char:
            - string1
            - string2
        Counter:
            - s1
            - s2
        Integer:
            - length1
            - length2
            - index
     */
    char string1[64], string2[64];
    counter s1, s2;
    int length1, length2, index;

    /*
        Input Section
        prompt user to input the first and second string
     */
    scanf(" %s", &string1);
    scanf(" %s", &string2);

    /*
        Measure the length of inputted string
     */
    length1 = strlen(string1);
    length2 = strlen(string2);

    /*
        Integer variable initialization
     */
    s1.vocal = s1.consonant = s2.vocal = s2.consonant = 0;

    /*
        Check each char of first string, wheter it's a vocal or a consonant
        If match, incement the matching counter variable
     */
    for (index = 0; index < length1; index += 1)
    {
        if (string1[index] == 'a' || string1[index] == 'i' || string1[index] == 'u' || string1[index] == 'e' || string1[index] == 'o' )
        {
            s1.vocal += 1;
        } else
        {
            s1.consonant += 1;
        }
    }

    /*
        Check each char of second string, wheter it's a vocal or a consonant
        If match, incement the matching counter variable
     */
    for (index = 0; index < length2; index += 1)
    {
        if (string2[index] == 'a' || string2[index] == 'i' || string2[index] == 'u' || string2[index] == 'e' || string2[index] == 'o' )
        {
            s2.vocal += 1;
        } else
        {
            s2.consonant += 1;
        }
    }

    /*
        Check each counter, the consonant and vocal.
        If the count result are equal, print matching condition.
        Otherwise, print "tidak ada yang sama" (nothing match).

        Consonant checked first, so if the consonant and vocal both equal, only consonant printed
     */
    if (s1.consonant == s2.consonant)
    {
        printf("sama konsonan\n");
    } else if (s1.vocal == s2.vocal)
    {
        printf("sama vokal\n");
    } else
    {
        printf("tidak ada yang sama\n");
    }

    /*
        Return integer of 0 to the program as the program succeed
     */
    return 0;
}