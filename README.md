# Algorithm & Programming 1 Course Source Code

This is a bunch of source code i write for Algorithm & Programming 1 Course at
UPI.

The Course using C Language.

## File Structure

- Latihan Alpro 1, CSPC's Competition.
- Latihan Online, CSPC's Competition.
- Meet, what i write on class.
- Praktikum, what i write on practice class.
- Responsi, what i write on responsi class.
- Tugas Praktikum, Practice Class Assignment.
- UTS, Mid-semester test.