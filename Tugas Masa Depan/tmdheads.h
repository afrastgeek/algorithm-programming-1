/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada tugas masa depan Algoritma dan Pemrograman I pada
 *  saat mengerjakan Tugas Masa Depan Algoritma dan Pemrograman I. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Header File
|--------------------------------------------------------------------------
|
| Here is where i register all needed libraries, global variable, global
| struct, and function (and procedure, if you distinguish it from function ;).
|
*/

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------------*/
/* Type and Structure declarations                                          */
/*--------------------------------------------------------------------------*/
/**
 * @brief The pattern block properties.
 * @details This struct hold an array of 5 integer which will be filled by
 * lastRowSpace function.
 * 
 */
typedef struct blockProperties {    /* Struct declaration */
    int last_space_t[5]; /* Struct definition, last space should be printed */
} blockProperties;  /* Altering Struct with Typedef declaration */


/*--------------------------------------------------------------------------*/
/* Variable declarations                                                    */
/*--------------------------------------------------------------------------*/
int g_index1, g_index2, g_index3;   /* Loop Counter, used in MAIN */
int g_row, g_column, g_block;   /* Pattern Loop Counter, used in PROGRAM */


/*--------------------------------------------------------------------------*/
/* Macro declarations                                                       */
/*--------------------------------------------------------------------------*/
#define NUMOFPATTERN (9)    /* Translate the Pattern number */


/*--------------------------------------------------------------------------*/
/* Function prototypes                                                      */
/*--------------------------------------------------------------------------*/
blockProperties lastRowSpace (int num_of_pure, int pure_message[]);

int shouldSpace (int block, int last_space_t);


/*--------------------------------------------------------------------------*/
/* Procedure prototypes                                                     */
/*--------------------------------------------------------------------------*/
void oooPattern (int thickness, int mustSpace);
void ooiPattern (int thickness, int mustSpace);
void oioPattern (int thickness, int mustSpace);
void oiiPattern (int thickness, int mustSpace);
void iooPattern (int thickness, int mustSpace);
void ioiPattern (int thickness, int mustSpace);
void iioPattern (int thickness, int mustSpace);
void iiiPattern (int thickness, int mustSpace);

void spaces (int thickness, int g_block, int mustSpace);

void fullRowPattern (int num_of_pure, int pure_message[num_of_pure],
    int thickness, int currentRow, int last_space_t);
void fullPattern (int num_of_pure, int pure_message[num_of_pure],
    int thickness, blockProperties x);
