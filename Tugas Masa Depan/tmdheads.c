/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada tugas masa depan Algoritma dan Pemrograman I pada
 *  saat mengerjakan Tugas Masa Depan Algoritma dan Pemrograman I. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Program File
|--------------------------------------------------------------------------
|
| Here is where i define all function and procedure*.
|
| *i know you're distinguishing it :)
|
*/

#include "tmdheads.h"

/**
 * @brief Find last filled pattern location.
 * @details This function are finding the last filled pattern location by
 * checking each message ID. The ID of zero (refer to "tolong") and six (refer
 * to "aman") --see main file-- were used because this message pattern has
 * empty row in the block.
 * 
 * @param num_of_pure Number of Message to be checked.
 * @param pure_message The array of Message ID.
 * 
 * @return Return the struct of array with last filled pattern location.
 */
blockProperties lastRowSpace (int num_of_pure, int pure_message[num_of_pure])
{
    int status; /* the status toggle variable */
    int line;   /* the line loop counter */
    blockProperties x;  /* structed variable, holding the calculated int */

    x.last_space_t[2] = x.last_space_t[3] = num_of_pure - 1; /*Default value*/

    for (line = 0; line < 5; line += 1) {   /* Foreach pattern line */
        status = 1; /* set the status */
        g_index1 = num_of_pure; /*hold the number of message on loop counter*/
        
        switch (line) { /* switch the line */
            case 0 : {
                do {
                    g_index1 -= 1; /* make the counter able to access array */
                    if (pure_message[g_index1] == 0 /* when "tolong" */
                        || pure_message[g_index1] == 6) { /* or "aman" */
                        x.last_space_t[0] = g_index1; /* get the index */
                    } else {    /* otherwise */
                        x.last_space_t[0] = g_index1; /* get the index */
                        status = 0; /* but stop the loop */
                    }
                } while (status && g_index1);
                break;
            } /*case 0*/
            case 1 : {
                do {
                    g_index1 -= 1; /* make the counter able to access array */
                    if (pure_message[g_index1] == 6) { /* when "aman" */
                        x.last_space_t[1] = g_index1; /* get the index */
                    } else {    /* otherwise */
                        x.last_space_t[1] = g_index1; /* get the index */
                        status = 0; /* but stop the loop */
                    }
                } while (status && g_index1);
                break;
            } /*case 1*/
/*
I've got a story here
ammarfr.wordpress.com/2016/01/01/assignment-for-better-future-initialization/
*/
            case 4 : {
                do {
                    g_index1 -= 1; /* make the counter able to access array */
                    if (pure_message[g_index1] == 0) { /* when "tolong" */
                        x.last_space_t[4] = g_index1; /* get the index */
                    } else {    /* otherwise */
                        x.last_space_t[4] = g_index1; /* get the index */
                        status = 0; /* but stop the loop */
                    }
                } while (status && g_index1);
                break;
            } /*case 4*/
        } /*switch the line*/
    } /*foreach pattern line*/

    return x;
} /*lastRowSpace function*/


/**
 * @brief is the block should print space?
 * @details This Function return the status condition for the pattern to
 * decide, whether print the spaces or not?
 * 
 * @param g_block The current block.
 * @param last_space_t The last space location.
 * 
 * @return Return value 1 when it should print the space, and vice versa.
 */
int shouldSpace (int g_block, int last_space_t)
{
    int must_space = 1;

    if (g_block >= last_space_t) {
        must_space = 0;
    }

    return must_space;
}


/*--------------------------------------------------------------------------*/
/* PATTERN PROCEDURE DECLARATION                                            */
/*--------------------------------------------------------------------------*/

/**
 * @brief The space-space-space pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void oooPattern (int thickness, int must_space)
{
    g_column = 0;
    while ( must_space && g_column < thickness * 3 ) {
        printf(" ");
        g_column += 1;
    }
}

/**
 * @brief The space-space-oh pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void ooiPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf(" ");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf(" ");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
}

/**
 * @brief The space-oh-space pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void oioPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf(" ");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    g_column = 0;
    while ( must_space && g_column < thickness ) {
        printf(" ");
        g_column += 1;
    }
}

/**
 * @brief The space-oh-oh pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void oiiPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf(" ");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
}

/**
 * @brief The oh-space-space pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void iooPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    g_column = 0;
    while ( must_space && g_column < thickness * 2) {
        printf(" ");
        g_column += 1;
    }
}

/**
 * @brief The oh-space-oh pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void ioiPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf(" ");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
}

/**
 * @brief The oh-oh-space pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void iioPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    g_column = 0;
    while ( must_space && g_column < thickness ) {
        printf(" ");
        g_column += 1;
    }
}

/**
 * @brief The oh-oh-oh pattern
 * 
 * @param thickness How much it should printed?
 * @param must_space Should i print the space?
 */
void iiiPattern (int thickness, int must_space)
{
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
    for (g_column = 0; g_column < thickness; g_column += 1) {
        printf("0");
    }
}

/**
 * @brief The space patter procedure.
 * 
 * @param thickness How much it should printed?
 * @param g_block Is it the last
 * @param must_space Should i print the space?
 */
void spaces (int thickness, int g_block, int must_space)
{
    g_column = 0;
    while (g_column < thickness && must_space) {
        printf(" ");
    g_column += 1;
    }
}

/**
 * @brief The Row Pattern Procedure
 * @details Selecting the row and print it to the screen
 * 
 * @param num_of_pure How much real cryptic message?
 * @param pure_message The cryptic message accessor.
 * @param thickness How much it should printed?
 * @param line The row to print
 * @param last_space_t Which is the last space?
 */
void fullRowPattern (int num_of_pure, int pure_message[num_of_pure], int thickness,
    int line, int last_space_t)
{
    g_row = 0;
    while (g_row < thickness && line == 0) {
        for (g_block = 0; g_block < num_of_pure; g_block += 1) {
            int must_space = shouldSpace(g_block, last_space_t);
            switch (pure_message[g_block]) {
                case 0 : { oooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 1 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 2 : { ooiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 3 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 4 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 5 : { iooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 6 : { oooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 7 : { iioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 8 : { iooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
            }
        }
        printf("\n");
        g_row += 1;
    }

    g_row = 0;
    while (g_row < thickness && line == 1) {
        for (g_block = 0; g_block < num_of_pure; g_block += 1) {
            int must_space = shouldSpace(g_block, last_space_t);
            switch (pure_message[g_block]) {
                case 0 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 1 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 2 : { ooiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 3 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 4 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 5 : { iooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 6 : { oooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 7 : { ooiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 8 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
            }
        }
        printf("\n");
        g_row += 1;
    }

    g_row = 0;
    while (g_row < thickness && line == 2) {
        for (g_block = 0; g_block < num_of_pure; g_block += 1) {
            int must_space = shouldSpace(g_block, last_space_t);
            switch (pure_message[g_block]) {
                case 0 : { iiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 1 : { iiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 2 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 3 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 4 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 5 : { iioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 6 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 7 : { iioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 8 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
            }
        }
        printf("\n");
        g_row += 1;
    }

    g_row = 0;
    while (g_row < thickness && line == 3) {
        for (g_block = 0; g_block < num_of_pure; g_block += 1) {
            int must_space = shouldSpace(g_block, last_space_t);
            switch (pure_message[g_block]) {
                case 0 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 1 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 2 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 3 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 4 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 5 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 6 : { ioiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 7 : { ooiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 8 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
            }
        }
        printf("\n");
        g_row += 1;
    }

    g_row = 0;
    while (g_row < thickness && line == 4) {
        for (g_block = 0; g_block < num_of_pure; g_block += 1) {
            int must_space = shouldSpace(g_block, last_space_t);
            switch (pure_message[g_block]) {
                case 0 : { oooPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 1 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 2 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 3 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 4 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 5 : { iioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 6 : { oiiPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 7 : { iioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
                case 8 : { oioPattern (thickness, must_space); spaces (thickness, g_block, must_space); break; }
            }
        }
        printf("\n");
        g_row += 1;
    }
}

/**
 * @brief Execute the fullRowPattern Procedure
 * @details This funtion called directly on the MAIN PROGRAM. On the loop of
 * five pattern line, print the pattern with the arguments needed.
 * 
 * @param num_of_pure How much real cryptic message?
 * @param pure_message The cryptic message accessor.
 * @param thickness How much it should printed?
 * @param x The found result of lastRowSpace function.
 */
void fullPattern (int num_of_pure, int pure_message[num_of_pure], int thickness,
    blockProperties x)
{
    int line;
    for (line = 0; line < 5; line += 1) {
        fullRowPattern (num_of_pure, pure_message, thickness, line,
            x.last_space_t[line]);
    }
}
