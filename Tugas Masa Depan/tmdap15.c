/*
 *  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang
 *  dispesifikasikan pada tugas masa depan Algoritma dan Pemrograman I pada
 *  saat mengerjakan Tugas Masa Depan Algoritma dan Pemrograman I. Jika saya
 *  melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 *  menerima hukumannya. Aamiin.
 */
/*
|--------------------------------------------------------------------------
| Main Program File
|--------------------------------------------------------------------------
|
| Here is where i build the program mainframe.
|
*/

#include "tmdheads.h"

/**
 * @brief main program function
 * @details This function are the main program function, which every local
 * variable declaration, user input prompt, and pattern display are executed.
 * 
 * @param argc The parameter are additional. void also could be used.
 * @param argv The parameter are additional. void also could be used.
 * 
 * @return Based on ISO C 9899:1999, This function should return an int
 * because we're running it on hosted environment (on top of an OS).
 */
int main(int argc, char const *argv[]) {
    /**
     * @brief Declaration Section
     */
    int num_of_string;  /* The number of full message  */
    int selection; /* number of message selection */
    int num_of_selection;  /* The selected message array access */
    int thickness;  /* Message Pattern Thickness */
    int num_of_pure;    /* The number of real cryptic message */
    int pure_message[128];  /* Array of index access to cryptic message */

    char known_message[NUMOFPATTERN][16] = {    /* The real cryptic message */
        "tolong",
        "kirim",
        "berita",
        "kosong",
        "matamata",
        "pasukan",
        "aman",
        "masuk",
        "tahan"
    }; /*char known_message*/

    /**
     * @brief Message Section
     */
    scanf("%d", &num_of_string); /* prompt input the num of string, max 100 */

    if (num_of_string > 100) {
        num_of_string = 100;    /* set to max 100 */
    } /*if 100*/

    char message[num_of_string][64];    /* message array declaration */

    for (g_index1 = 0; g_index1 < num_of_string; g_index1 += 1) {
        scanf(" %s", message[g_index1]);    /* prompt input the message */
    } /*for num_of_string*/

    /**
     * @brief Selection Section
     */
    scanf("%d", &num_of_selection);    /* prompt input the num of selection */

    g_index3 = 0;
    for (g_index1 = 0; g_index1 < num_of_selection; g_index1 += 1) {
        scanf("%d", &selection);   /* prompt to input the selection */
        selection -= 1; /*convert the human counter to computer counter*/

        /**
         * @brief Get the access to known message from selection
         */
        for (g_index2 = 0; g_index2 < NUMOFPATTERN; g_index2 += 1) {
            /* if the string match the known message pattern, copy string */
            if (strcmp(message[selection], known_message[g_index2]) == 0) {
                pure_message[g_index3] = g_index2;  /* copy the string */

                g_index3 += 1;  /* increment the counter */
            } /*if match*/
        } /*for NUMOFPATTERN*/
    } /*for num_of_selection*/
    num_of_pure = g_index3; /* Store the number of real cryptic message */

    /**
     * @brief Thickness Section
     */
    scanf("%d", &thickness); /*prompt input the thickness of pattern, max 20*/

    if (thickness > 20) {
        thickness = 20; /* set to max 20 */
    } /*if 20*/

    /**
     * @brief String Display Section
     */
    for (g_index1 = 0; g_index1 < num_of_pure; g_index1 += 1) {
        printf("%s", known_message[pure_message[g_index1]]);
        if (g_index1 != num_of_pure - 1) {
            printf(" ");    /* no space at the end */
        } /*if not the end*/
    } /*for num_of_pure*/
    printf("\n");

    for (g_index1 = 0; g_index1 < thickness; g_index1 += 1) {
        printf("\n");   /* print enter character in number of thickness */
    } /*for thickness*/

    /**
     * @brief Pattern Display Section
     */
    /* Get the last filled pattern block, no more spaces! */
    blockProperties x = lastRowSpace (num_of_pure, pure_message);

    fullPattern (num_of_pure, pure_message, thickness, x);  /* the pattern */

    return 0; /* return 0 as the program succeed*/
} /*main function*/
