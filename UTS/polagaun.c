/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan UTS Alpro I, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	/*
		Define variable for width of dress and indexing
	 */
	int width, index, anotherindex;

	/*
		Prompt user to input the width of dress
	 */
	scanf("%d", &width);

	/*
		First row group
	 */
	for (index = 0; index < width; index += 1)
	{
		/*
			Print space of inversed triangle
		 */
		for (anotherindex = width - 1; anotherindex > index; anotherindex -= 1)
		{
			printf(" ");
		}

		/*
			Print shape of triangle
		 */
		for (anotherindex = 0; anotherindex <= index; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print shape of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print space of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf(" ");
		}

		/*
			Print shape of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print shape of triangle
		 */
		for (anotherindex = 0; anotherindex <= index; anotherindex += 1)
		{
			printf("*");
		}
		
		/*
			Print enter for each row
		 */
		printf("\n");
	}

	/*
		Second row group
	 */
	for (index = 0; index < width; index += 1)
	{
		/*
			Print space of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf(" ");
		}

		/*
			Print space of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf(" ");
		}

		/*
			Print shape of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print enter for each row
		 */
		printf("\n");
	}

	/*
		Third row group
	 */
	for (index = 0; index < width; index += 1)
	{
		/*
			Print space of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf(" ");
		}

		/*
			Print space of inversed triangle
		 */
		for (anotherindex = width - 1; anotherindex > index; anotherindex -= 1)
		{
			printf(" ");
		}

		/*
			Print shape of triangle
		 */
		for (anotherindex = 0; anotherindex <= index; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print shape of square
		 */
		for (anotherindex = 0; anotherindex < width; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print shape of triangle
		 */
		for (anotherindex = 0; anotherindex <= index; anotherindex += 1)
		{
			printf("*");
		}

		/*
			Print enter for each row
		 */
		printf("\n");
	}

	/*
		Return 0 if program success
	 */
	return 0;
}