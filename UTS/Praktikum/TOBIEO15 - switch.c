#include <stdio.h>

int main()
{
    int num, target, golden, temporary, index, anotherindex, otheranotherindex, count;
    char command;
    
    count = 0;

    scanf("%d", &num);

    int chair[num + 1];

    for (index = 0; index < num; index += 1)
    {
        scanf("%d", &chair[index]);
    }

    scanf("%d", &target);

    for (index = 0; index < target; index += 1)
    {
        scanf(" %c %d", &command, &golden);

        switch(command){
            case 'i': {
                for (anotherindex = 0; anotherindex < num; anotherindex += 1)
                {
                    if (chair[anotherindex] == golden)
                    {
                        for (otheranotherindex = num + 1; otheranotherindex > anotherindex; otheranotherindex -= 1)
                        {
                            chair[otheranotherindex] = chair[otheranotherindex - 1];
                        }

                        for (otheranotherindex = 0; otheranotherindex < num + 1; otheranotherindex += 1)
                        {
                            printf("%d-", chair[otheranotherindex]);
                        }
                        printf("\n");

                        chair[anotherindex] = golden * 100;

                        for (otheranotherindex = 0; otheranotherindex < num + 1; otheranotherindex += 1)
                        {
                            printf("%d.", chair[otheranotherindex]);
                        }
                        printf("\n");
                    }
                }


                break;
            }
            case 'd': {
                for (anotherindex = 0; anotherindex < num; anotherindex += 1)
                {
                    if (chair[anotherindex] == golden)
                    {
                        chair[anotherindex] = chair[anotherindex + 1];
                    }
                }

                break;
            }
            case 'c': {
                for (anotherindex = 0; anotherindex < num; anotherindex += 1)
                {
                    if (chair[anotherindex] != golden)
                    {
                        count += 1;
                    }
                }
                
                break;
            }
        }
    }

    count = count + 1;

    for (index = 0; index < count; index += 1)
    {
        printf("%d\n", chair[index]);
    }

    printf("count: %d\n", count);

    return 0;
}