/*  Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *  dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *  mengerjakan Evaluasi Praktikum Alpro I, jika saya melakukan kecurangan
 *  maka Allah/Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya.
 *  Aamiin
 */

/*
    Include standard input output library for scanf and printf function
 */
#include <stdio.h>

/*
    Create main function for the program to run
 */
int main()
{
    /*
        Create integer variable for
        -   length of the array (the number of chair)
        -   number of instruction
        -   instruction for target chair
        -   element of the array (the target chair)
        -   index and inner_index for looping condition
        -   count variable for counting position of array element (the chair)
     */
    int length, instruction, target, element, index, inner_index, count = 0;
    
    /*
        Create character variable to store the command value
     */
    char command;
    
    /*
        Prompt user to input the number of special chair,
        or the length of the array.
     */
    scanf("%d", &length);

    /*
        Create array of chair with specified length by user input
     */
    int chair[length];

    /*
        Create loop condition for prompting user to input the special chair
        number. Fill the array with user input corresponding with length of the array.
     */
    for (index = 0; index < length; index += 1)
    {
        /*
            Prompt user to input the special chair number
         */
        scanf("%d", &chair[index]);
    }

    /*
        Prompt user to input the number of instruction to manage the chair.
     */
    scanf("%d", &instruction);

    /*
        Create loop for processing the instruction corresponding with number of instruction specified.
     */
    for (index = 0; index < instruction; index += 1)
    {
        /*
            Prompt user to input the command and the target chair
         */
        scanf(" %c %d", &command, &target);

        /*
            Create loop to get the element number of the target chair.
         */
        for (inner_index = 0; inner_index < length; inner_index += 1)
        {
            /*
                Check whether the array element is the command target
             */
            if (chair[inner_index] == target)
            {
                /*
                    Define the element variable with the value of element index
                 */
                element = inner_index;
            }
        }

        /*
            THE INSERT COMMAND
            Check whether the user asking for inserting a number to the array
         */
        if (command == 'i')
        {
            /*
                Because it's an insert command, add one more length to the
                array
             */
            length += 1;

            /*
                Create loop for moving the array element from the end of the
                array
             */
            /*for (inner_index = length; inner_index > element; inner_index -= 1)*/
            inner_index = length;
            while (inner_index > element && status != 1)
            {
                /*
                    Move the array element one step higher
                    [lower, current_position, higher]
                 */
                chair[inner_index] = chair[inner_index - 1];

                /* Iteration for Inner Index */
                inner_index += 1;
            }

            /*
                "Inserting" a value to the array

                Actually, it's changing array element value after the target
                element.
             */
            chair[element + 1] = target * 100;
        }

        /*
            THE DELETE COMMAND
            Check whether the user asking for deleting a number from the array
         */
        else if (command == 'd')
        {
            /*
                Create loop to replace the target element value with higher
                element value
             */
            for (inner_index = element; inner_index < length; inner_index += 1)
            {
                /*
                    Replace the target element with higher element value
                 */
                chair[inner_index] = chair[inner_index + 1];
            }

            /*
                Because it's delete command, remove one length from the array
             */
            length -= 1;
        }

        /*
            THE COUNT COMMAND
            Check whether the user asking for counting the element from the
            array
         */
        else if (command == 'c')
        {
            /*
                Create loop to count the target element from the start of array
             */
            for (inner_index = 0; inner_index <= element; ++inner_index)
            {
                /*
                    Increment the count variable
                 */
                count += 1;
            }
        }
    }

    /*
        PRINT SECTION
        Create loop for displaying the array corresponding with length of the
        array
     */
    for (index = 0; index < length; index += 1)
    {
        /*
            Print the array element
         */
        printf("%d\n", chair[index]);
    }

    /*
        Create conditional for checking whether the user asking for counting
        the element position
     */
    if (command == 'c')
    {
        /*
            Print the element position based on inceremented count variable
         */
        printf("count: %d\n", count);
    }

    /*
        What to return if the program success
     */
    return 0;
}