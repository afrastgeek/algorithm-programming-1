/*	Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan seperti yang telah
 *	dispesifikasikan pada mata kuliah Algoritma dan Pemrograman I dalam
 *	mengerjakan UTS Alpro I, jika saya melakukan kecurangan maka Allah/
 *	Tuhan adalah saksi saya, dan saya bersedia menerima hukumanNya. Aamiin
 */
#include <stdio.h>

int main()
{
	/*
		Create integer variable for storing user defined value, and index.
	 */
	int count, half_count, index;

	/*
		Prompt user to input the value
	 */
	scanf("%d", &count);

	/*
		Create array of integer with name "number" with length of count, the
		user defined value.
	 */
	int number[count];

	/*
		Create loop of input in number of count, the user defined value.
	 */
	for (index = 0; index < count; index += 1)
	{
		scanf("%d", &number[index]);
	}

	/*
		Divide the count by 2, to get the half last and half first of array.
	 */
	half_count = count / 2;

	/*
		Create loop for displaying the half last of array
	 */
	for (index = half_count; index < count; index += 1)
	{
		printf("%d\n", number[index]);
	}

	/*
		Create loop for displaying the half first of array
	 */
	for (index = 0; index < half_count; index += 1)
	{
		printf("%d\n", number[index]);
	}	

	return 0;
}