#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {
    int size_h, size_v, index, index2, currentLength, tilesAccessor;

    scanf("%d %d", &size_v, &size_h);

    char field[size_v][size_h];
    int mineCounter[10000] = { 0 };

    for (index = 0; index < size_v; index += 1) {
        scanf(" %s", field[index]);
    }

    tilesAccessor = 0;
    for (index = 0; index < size_v; index += 1) {
        for (index2 = 0; index2 < size_h; index2 += 1) {
            if (index == 0) {
                if (index2 == 0) {
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else if (index2 == size_h - 1) {
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else {
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                }
            } else if (index == size_v - 1) {
                if (index2 == 0) {
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else if (index2 == size_h - 1) {
                    if (field[index - 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else {
                    if (field[index - 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                }
            } else {
                if (index2 == 0) {
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else if (index2 == size_h - 1) {
                    if (field[index - 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                } else {
                    if (field[index - 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index - 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 - 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                    if (field[index + 1][index2 + 1] == '*') {
                        mineCounter[tilesAccessor] += 1;
                    }
                }
            }
            tilesAccessor += 1;
        }

        tilesAccessor -= size_h;
        for (index2 = 0; index2 < size_h; index2 += 1) {
            if (field[index][index2] == '*') {
                printf("*");
            } else {
                printf("%d", mineCounter[tilesAccessor]);
            }
            tilesAccessor += 1;
        }
        printf("\n");

        tilesAccessor += size_h;
    }

    return 0;
}
