#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {
    int numOfTag, index, unpaired, paired;

    unpaired = paired = 0;

    scanf("%d", &numOfTag);

    char tag[numOfTag][8];

    for (index = 0; index < numOfTag; index += 1) {
        scanf(" %s", tag[index]);
        if (strcmp(tag[index], "<div>") == 0 && unpaired != -1)
        {
            unpaired += 1;
        }
        if (strcmp(tag[index - 1], "<div>") == 0 && strcmp(tag[index], "</div>") == 0)
        {
            unpaired -= 1;
            paired += 1;
        } else if (strcmp(tag[index - (paired * 2)], "<div>") == 0 && strcmp(tag[index], "</div>") == 0)
        {
            unpaired -= 1;
            paired += 1;
        }
    }

    if (unpaired == 0) {
        printf("valid\n");
    } else {
        printf("tidak valid\n");
    }


    return 0;
}
