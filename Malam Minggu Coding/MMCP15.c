#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {
    int numOfString, index, index2, currentLength, divided, divideInt, prime;

    float divideFloat;

    scanf("%d", &numOfString);

    int sum[numOfString];
    char mixed[numOfString][64];

    prime = 0;
    for (index = 0; index < numOfString; index += 1) {
        scanf(" %s", mixed[index]);
        sum[index] = 0;
        currentLength = strlen(mixed[index]);
        for (index2 = 0; index2 < currentLength; index2 += 1) {
            if (mixed[index][index2] == '0' || mixed[index][index2] == '1'
                || mixed[index][index2] == '2'  || mixed[index][index2] == '3'
                || mixed[index][index2] == '4'  || mixed[index][index2] == '5'
                || mixed[index][index2] == '6'  || mixed[index][index2] == '7'
                || mixed[index][index2] == '8'  || mixed[index][index2] == '9' ) {
                sum[index] += (mixed[index][index2] - 48);
            }
        }

        divided = 0;

        for (index2 = 1; index2 <= sum[index]; index2 += 1) {
            divideInt = sum[index] / index2;
            divideFloat = ((sum[index] / (float)index2) - divideInt) * 1000;
            if (divideFloat == 0) {
                divided+= 1;
            }
        }

        if (divided == 2) {
            prime += 1;
        }
    }

    if (prime >= 2) {
        printf("valid\n");
    } else {
        printf("tidak valid\n");
    }

    return 0;
}
