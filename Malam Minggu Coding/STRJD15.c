#include <stdio.h>
#include <string.h>

typedef struct name {
    char firstName[64];
    char lastName[64];
} name;

int main(int argc, char const *argv[])
{
    name first, second;
    int lengthStore1, lengthStore2, lengthStore3, lengthStore4;
    int index, consCounter1, consCounter2;

    consCounter1 = consCounter2 = 0;

    scanf(" %s %s", first.firstName, first.lastName);
    scanf(" %s %s", second.firstName, second.lastName);

    lengthStore1 = strlen(first.firstName);
    lengthStore2 = strlen(second.firstName);
    lengthStore3 = strlen(first.lastName);
    lengthStore4 = strlen(second.lastName);

    for (index = 0; index < lengthStore3; index += 1) {
        if (first.lastName[index] != 'a' && first.lastName[index] != 'i'
            && first.lastName[index] != 'u' && first.lastName[index] != 'e'
            && first.lastName[index] != 'o') {
            consCounter1 += 1;
        }
    }

    for (index = 0; index < lengthStore4; index += 1) {
        if (second.lastName[index] != 'a' && second.lastName[index] != 'i'
            && second.lastName[index] != 'u' && second.lastName[index] != 'e'
            && second.lastName[index] != 'o') {
            consCounter2 += 1;
        }
    }

    if (lengthStore1 == lengthStore2 && consCounter1 == consCounter2) {
        printf("KUA yuk\n");
    } else {
        printf("dadah :)\n");
    }

    return 0;
}
