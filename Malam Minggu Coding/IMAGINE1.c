#include <stdio.h>
#include <string.h>

typedef struct passwordHistory {
    char first[64];
    char last[16];
} passwordHistory;

int main(int argc, char const *argv[]) {
    int numOfString, position, index, index2;

    scanf("%d", &numOfString);

    passwordHistory history[numOfString], password[numOfString];

    index2 = 0;
    for (index = 0; index < numOfString; index += 1) {
        scanf(" %s %s", history[index].first, history[index].last);
        if (strcmp(history[index].last, "it") == 0) {
            strcpy(password[index2].first, history[index].first);
            index2 += 1;
        }
    }

    scanf("%d", &position);

    printf("%s\n", password[position - 1].first);

    return 0;
}