#include <stdio.h>

int main(int argc, char const *argv[])
{
    int numOfInt, index, index2, counter[10] = { 0 };

    scanf("%d", &numOfInt);

    int history[numOfInt];

    for (index = 0; index < numOfInt; index += 1)
    {
        scanf("%d", &history[index]);

        switch(history[index]){
            case 0 : counter[0] += 1; break;
            case 1 : counter[1] += 1; break;
            case 2 : counter[2] += 1; break;
            case 3 : counter[3] += 1; break;
            case 4 : counter[4] += 1; break;
            case 5 : counter[5] += 1; break;
            case 6 : counter[6] += 1; break;
            case 7 : counter[7] += 1; break;
            case 8 : counter[8] += 1; break;
            case 9 : counter[9] += 1; break;
        }
    }

    for (index = 0; index < 10; index += 1)
    {
        if (counter[index])
        {
        printf("%d ", index);
        for (index2 = 0; index2 < counter[index]; index2 += 1)
        {
            printf("*");
        }
        printf("\n");
        }
    }

    return 0;
}