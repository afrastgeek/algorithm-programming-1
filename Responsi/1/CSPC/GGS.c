#include <stdio.h>

int main()
{
	int a, b, c;

	scanf("%d %d %d", &a, &b, &c);

	if (a % 2 != 0)
	{
		printf("%d ganjil\n", a);
	} else
	{
		printf("%d genap\n", a);
	}

	if (b % 2 != 0)
	{
		printf("%d ganjil\n", b);
	} else
	{
		printf("%d genap\n", b);
	}

	if (c % 2 != 0)
	{
		printf("%d ganjil\n", c);
	} else
	{
		printf("%d genap\n", c);
	}

	return 0;
}