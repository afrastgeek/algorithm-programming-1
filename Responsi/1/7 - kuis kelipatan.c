#include <stdio.h>

int main()
{
	int angka1, angka2, angka3;

	scanf("%d %d %d", &angka1, &angka2, &angka3);

	if (angka1 % angka2 == 0)
	{
		printf("%d %d\n", angka1, angka2);
	}

	if (angka1 % angka3 == 0)
	{
		printf("%d %d\n", angka1, angka3);
	}
	
	if (angka2 % angka1 == 0)
	{
		printf("%d %d\n", angka2, angka1);
	}

	if (angka2 % angka3 == 0)
	{
		printf("%d %d\n", angka2, angka3);
	}

	if (angka3 % angka1 == 0)
	{
		printf("%d %d\n", angka3, angka1);
	}

	if (angka3 % angka2 == 0)
	{
		printf("%d %d\n", angka3, angka2);
	}

	return 0;
}