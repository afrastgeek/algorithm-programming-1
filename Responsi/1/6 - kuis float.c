/*
 *	dari 4 input angka float,
 *	Tampilkan yang bukan angka terendah & tertinggi dengan presisi 2 angka di belakang koma.
 */
#include <stdio.h>

int main()
{
	float angka1, angka2, angka3, angka4;

	// float angka1
	// float angka2
	// float angka3
	// float angka4
	
	scanf("%f %f %f %f", &angka1, &angka2, &angka3, &angka4);

	if ((angka1 > angka2) && (angka1 > angka3) && (angka1 > angka4))
	{
		if ((angka2 < angka3) && (angka2 < angka4))
		{
			printf("%.2f %.2f\n", angka3, angka4);
		} else if ((angka3 < angka2) && (angka3 < angka4))
		{
			printf("%.2f %.2f\n", angka2, angka4);
		} else if ((angka4 < angka2) && (angka4 < angka3))
		{
			printf("%.2f %.2f\n", angka2, angka3);
		} 
	} else if ((angka2 > angka1) && (angka2 > angka3) && (angka2 > angka4))
	{
		if ((angka1 < angka3) && (angka1 < angka4))
		{
			printf("%.2f %.2f\n", angka3, angka4);
		} else if ((angka3 < angka1) && (angka3 < angka4))
		{
			printf("%.2f %.2f\n", angka1, angka4);
		} else if ((angka4 < angka1) && (angka4 < angka3))
		{
			printf("%.2f %.2f\n", angka1, angka3);
		} 
	}else if ((angka3 > angka1) && (angka3 > angka2) && (angka3 > angka4))
	{
		if ((angka1 < angka2) && (angka1 < angka4))
		{
			printf("%.2f %.2f\n", angka2, angka4);
		} else if ((angka2 < angka1) && (angka2 < angka4))
		{
			printf("%.2f %.2f\n", angka1, angka4);
		} else if ((angka4 < angka1) && (angka4 < angka2))
		{
			printf("%.2f %.2f\n", angka1, angka2);
		} 
	}else if ((angka4 > angka1) && (angka4 > angka2) && (angka4 > angka3))
	{
		if ((angka1 < angka2) && (angka1 < angka3))
		{
			printf("%.2f %.2f\n", angka2, angka3);
		} else if ((angka2 < angka1) && (angka2 < angka3))
		{
			printf("%.2f %.2f\n", angka1, angka3);
		} else if ((angka3 < angka1) && (angka3 < angka2))
		{
			printf("%.2f %.2f\n", angka1, angka2);
		} 
	}

	return 0;
}