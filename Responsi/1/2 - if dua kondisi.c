#include <stdio.h>

int main()
{
	//input
	int n;
	scanf("%d", &n);

	//pengecekan
	if (n%2 == 1)
	{
		printf("angka %d merupakan bilangan ganjil\n", n);
	} else
	{
		printf("angka %d merupakan bilangan genap\n", n);
	}

	return 0;
}