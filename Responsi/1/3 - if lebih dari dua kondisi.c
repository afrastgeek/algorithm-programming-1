#include <stdio.h>

int main()
{
	int suhu;
	printf("Masukkan besarnya suhu : ");
	scanf("%d", &suhu);

	/* Melakukan pengecekan terhadap suhu */
	printf("Pada suhu %d derajat Celcius, air berbentuk :\n", suhu);
	
	if (suhu <= 0)
	{
		printf("Padat (Es)\n");
	} else if ((suhu > 0) && (suhu < 100))
	{
		printf("Cairan\n");
	} else
	{
		printf("Gas\n");
	}

	return 0;
}