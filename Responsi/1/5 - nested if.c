#include <stdio.h>

int main()
{
	char jenis_kelamin;
	int tinggi;
	
	scanf(" %c", &jenis_kelamin);
	scanf("%d", &tinggi);

	if ((jenis_kelamin == 'l') || (jenis_kelamin == 'L'))
	{
		if (tinggi >= 170)
		{
			printf("Lulus\n");
		} else 
		{
			printf("Tidak Lulus\n");
		}
	} else if ((jenis_kelamin == 'p') || (jenis_kelamin == 'P'))
	{
		if (tinggi >= 160)
		{
			printf("Lulus\n");
		} else
		{
			printf("Tidak Lulus\n");
		}
	}

	return 0;
}