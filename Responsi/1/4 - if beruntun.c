#include <stdio.h>

int main()
{
	int n;
	scanf("%d", &n);

	/* Melakukan pengecekan */	
	if (n%2 == 1)
	{
		printf("%d merupakan angka ganjil\n", n);
	}

	if (n%3 == 0)
	{
		printf("%d merupakan bilangan kelipatan 3\n", n);
	}

	if (n%5 == 0)
	{
		printf("%d merupakan bilangan kelipatan 5\n", n);
	}

	if (n >= 0)
	{
		printf("%d merupakan bilangan bernilai positif\n", n);
	}

	return 0;
}