/*
    Menyertakan library header.h
*/
#include "header.h"

/**
 * Fungsi untuk mencari angka terbesar dari suatu kelompok angka.
 * 
 * @param n ,banyaknya angka di kelompok angka
 * @param paket ,array angka yang akan di cek
 * 
 * @return angka terbesar
 */
int getMax(int n, int paket[]){
    /*Algoritma Pencarian Max*/
    int temp = 0;
    int i;
    // loop sebanyak jumlah angka di kelompok angka
    for(i=0;i<n;i++){
        if(paket[i] > temp){
            // jika angka lebih besar dari temporary, ganti nilai temporary dengan angka tersebut
            temp = paket[i];
        }
    }

    return temp;
}