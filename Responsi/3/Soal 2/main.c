/*
    Menyertakan library header.h
*/
#include "header.h"

/*
    Fungsi utama program
*/
int main(){
    int i;

    // banyaknya angka di kelompok angka pertama
    int n;
    scanf("%d",&n);
    
    // Input untuk kelompok angka pertama sebanyak n
    int paket1[n];
    for(i=0;i<n;i++){
        scanf("%d",&paket1[i]);
    }

    // banyaknya angka di kelompok angka pertama
    int m;
    scanf("%d",&m);
    int paket2[m];

    // Input untuk kelompok angka pertama sebanyak m
    for(i=0;i<m;i++){
        scanf("%d",&paket2[i]);
    }

    /*
        Mencari angka terbesar dari kedua kelompok angka, kemudian simpan angka tersebut di variabel
    */
    int max1 = getMax(n,paket1);
    int max2 = getMax(m,paket2);

    /*
        Jika kedua angka terbesar genap, print valid
        Jika kedua angka terbesar ganjil, print valid
        Selain itu, print tidak valid
    */
    if(max1 % 2 == 0 && max2 % 2 == 0){
        printf("valid\n");
    }else if(max1 % 2 == 1 && max2 % 2 == 1){
        printf("valid\n");
    }else{
        printf("tidak valid\n");
    }
    return 0;
}