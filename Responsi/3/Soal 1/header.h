// array of array of char
// array of string

/*
    Menyertakan library stdio.h dan string.h
*/
#include <stdio.h>
#include <string.h>

int getKonsonan(char kuis[]);
int getVokal(char kuis[]);
void printVokal(char kuis[]);
void printKonsonan(char kuis[]);