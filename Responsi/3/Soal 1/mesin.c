/*
    Menyertakan library header.h
*/
#include "header.h"
// array of array of char
// array of char

/**
 * Fungsi untuk menghitung huruf vokal di string
 * 
 * @parameter kuis ,string yang mau di hitung huruf vokalnya.
 * @return jumlah huruf vokal di string tersebut
 */
int getVokal(char kuis[]){
    int panjangString = strlen(kuis);
    int i;
    int temp = 0;
    // loop sebanyak jumlah huruf (panjang string)
    for (i = 0; i < panjangString; ++i){
        if(kuis[i] == 'a' ||
            kuis[i] == 'i' ||
            kuis[i] == 'e' ||
            kuis[i] == 'u' ||
            kuis[i] == 'o'){
                // jika huruf tersebut vokal, tambahkan nilai variabel temp
                temp++;
            }
    }
    return temp;
}

/**
 * Fungsi untuk menghitung huruf konsonan di string
 * 
 * @parameter kuis ,string yang mau di hitung huruf konsonannya.
 * @return jumlah huruf konsonan di string tersebut
 */
int getKonsonan(char kuis[]){
    int panjangString = strlen(kuis);
    int i;
    int temp = 0;
    // loop sebanyak jumlah huruf (panjang string)
    for (i = 0; i < panjangString; ++i){
        if(kuis[i] != 'a' &&
            kuis[i] != 'i' &&
            kuis[i] != 'e' &&
            kuis[i] != 'u' &&
            kuis[i] != 'o'){
                // jika huruf tersebut bukan vokal (konsonan), tambahkan nilai variabel temp
                temp++;
            }
    }
    return temp;   
}

/**
 * Prosedur untuk menampilkan huruf vokal dari string.
 * 
 * @parameter kuis ,string yang mau di tampilkan huruf vokalnya.
 */
void printVokal(char kuis[]){
    int i;
    // loop sebanyak jumlah huruf (panjang string)
    for(i=0;i<strlen(kuis);i++){
        if(kuis[i] == 'a' ||
        kuis[i] == 'i' ||
        kuis[i] == 'e' ||
        kuis[i] == 'u' ||
        kuis[i] == 'o'){
            // jika vokal, print huruf tersebut
            printf("%c",kuis[i]);
        }
    }
}

/**
 * Prosedur untuk menampilkan huruf konsonan dari string.
 * 
 * @parameter kuis ,string yang mau di tampilkan huruf konsonannya.
 */
void printKonsonan(char kuis[]){
    int i;
    // loop sebanyak jumlah huruf (panjang string)
    for(i=0;i<strlen(kuis);i++){
          if(kuis[i] != 'a' &&
            kuis[i] != 'i' &&
            kuis[i] != 'e' &&
            kuis[i] != 'u' &&
            kuis[i] != 'o'){
            // jika bukan vokal (konsonan), print huruf tersebut
                printf("%c",kuis[i]);
            }
    }
}