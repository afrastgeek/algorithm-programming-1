/*
    Menyertakan library header.h
*/
#include "header.h"

/*
    Fungsi utama program
*/
int main()
{
    // deklarasi variable
    int i;
    
    // banyaknya string yang akan di-input
    int n;
    scanf("%d",&n);
    
    // membuat array of string sebanyak "n" dengan maksimal karakter 100
    char kuis[n][100];
    // loop untuk input string sebanyak "n"
    for(i=0;i<n;i++){
        scanf("%s",&kuis[i]);
    }

    /*
        Menghitung karakter menggunakan fungsi dan simpan hasilnya di variabel.
    */
    // memeriksa jumlah vokal dan konsonan pada string pertama
    int vokalAtas = getVokal(kuis[0]);
    int konsoAtas = getKonsonan(kuis[0]);
    // memeriksa jumlah vokal dan konsonan pada string terakhir
    int vokalBawah = getVokal(kuis[n-1]);
    int konsoBawah = getKonsonan(kuis[n-1]);

    /*
        Menampilkan karakter vokal dan konsonan menggunakan prosedur,
        APABILA jumlah karakter vokal string pertama dan string terakhir sama
        serta jumlah karakter konsonan string pertama dan string terakhir sama.
    */
    if((vokalAtas == vokalBawah) && (konsoAtas == konsoBawah)){
        // Menampilkan karakter vokal string pertama dan terakhir
        printVokal(kuis[0]);
        printVokal(kuis[n-1]);
        printf("\n");
        // Menampilkan karakter konsonan string pertama dan terakhir
        printKonsonan(kuis[0]);
        printKonsonan(kuis[n-1]);
        printf("\n");
    }else{
        // Apabila tidak memenuhi syarat, print "tidak sama"
        printf("tidak sama\n");
    }

    return 0;
}