Terdapat dua buah kelompok angka. Cari angka yang terbesar dari masing-masing
kelompok. Jika kedua angka terbesarnya ganjil-ganjil atau genap-genap, print valid. Jika berbeda, print tidak valid.

Contoh Masukan:
4       // banyaknya angka di kelompok pertama
7
8
12
14
6       // banyaknya angka di kelompok kedua
1
2
3
4
5
20

Contoh Masukan:
4
7
8
12
14
6
1
2
3
4
5
20

Contoh Keluaran:
valid